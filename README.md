# gfdmp

#### 项目介绍
MySQL备份/恢复，安装卸载，搭建MGR/半同步集群等自动化工具

本工具提供备份恢复mysql实例等功能：
- 支持全备。
- 支持增备。
- 支持全备恢复，且自动化恢复。
- 支持全备+增备恢复，且自动化恢复。

#### 安装教程

克隆项目

```
mkdir -p /data/git
cd /data/git
git clone http://gitlab.mljr.com/shan.mo01/gfdmp.git
```


拷贝配置文件到/etc

```
cd gfdmp
cp conf/gfdmp.conf /etc/gfdmp.conf        #/etc/gfdmp.conf是默认的配置文件，建议要存在，否则在执行时没有指定配置文件将出错
cp conf/gfdmp.conf /etc/gfdmp.conf.3332   #这步可选，对于多实例的环境有用，如果是单实例，直接使用/etc/gfdmp.conf即可,本文使用/etc/gfdmp.conf.3332作为demo

```
更新配置文件

```
vim /etc/gfdmp.conf.3332                       #根据实际情况编辑哪个配置文件

#请按照实际情况更新，且不要更新conf目录下的配置文件，这样在更新代码时不会出错
```
配置环境变量

```
echo "export PATH=/data/git/gfdmp/bin:${PATH}" >> /etc/profile
source /etc/profile

```

#### 使用说明

执行主程序

```
#第一次运行时间会比较久，会安装一些依赖程序
#如果没有xtrabackup环境则会安装，请耐心等待
[root@yz-mysql-101-68 conf]# gfdmp 
[2019-09-23 16:33:47] [INFO] Missing the necessary tools[rlwrap]. Check the environment for this OS
[2019-09-23 16:33:52] [INFO] Preparing the environment, please wait...

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       Management platform info for MySQL
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         Version: V_3.0
         Author : MoShan
         MySQL  : 5.6 or 5.7
         QQ     : 1005155691
         Mail   : mo_shan@yeah.net

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         MySQL current version : 5.6
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
1.  Backup full data for MySQL
2.  Backup increment data for MySQL
3.  Recover data to MySQL
4.  Recover data to MySQL and Create Replication
5.  Initialization profile
6.  Install MySQL
7.  Uninstall MySQL
8.  Install MySQL and Create MySQL Group Replication
9.  Install MySQL and Create Replication
10. EXIT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Please ENTER your choice [1-10]:

gfdmp /data/git/gfdmp/conf > 

```

配置定时任务

```
00 01 * * * /data/git/gfdmp/bin/gfdmp --type=backup --backup-plan=1,5 --backstage --defaults-file=/etc/gfdmp.conf.3332 --threads=10 --debug > /data/git/gfdmp/log/3332_backup.log 2>&1
```
> 参数的使用可以参考：gfdmp -h
- --type           操作类型，可选值有full | increment or incre | recover | recover_repl | install | uninstall | install_mysql_mgr | install_mysql_semi | backup
- --backup-plan    备份计划，按照星期做全备，要求--type=backup，否则不生效。如上表示周一和周五做全备，其余时间增备，不连续的星期逗号隔开，连续的可用-连接，如1-5表示周一到周五都做全备
- --backstage      是否放到后台，如果对于大数据量的环境建议使用，否则中途断开很闹心
- --defaults-file  配置文件，如果不指定配置文件会默认读取/etc/gfdmp.conf，多实例环境可指定
- --threads=       线程数，是否多线程压缩，解压时也是多线程解压(解密)
- --debug          debug模式，会将备份(恢复)等命令打印到日志

#### 其他使用

单纯备份MySQL

```
#配置好程序的配置文件后，执行程序，输入 1 即可备份
root ~ >> gfdmp 

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       Management platform info for MySQL
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         Version: V_3.0
         Author : MoShan
         MySQL  : 5.6 or 5.7
         QQ     : 1005155691
         Mail   : mo_shan@yeah.net

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         MySQL current version : 5.7
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
1.  Backup full data for MySQL
2.  Backup increment data for MySQL
3.  Recover data to MySQL
4.  Recover data to MySQL and Create Replication
5.  Initialization profile
6.  Install MySQL
7.  Uninstall MySQL
8.  Install MySQL and Create MySQL Group Replication
9.  Install MySQL and Create Replication
10. EXIT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Please ENTER your choice [1-10]:

gfdmp /root > 1

```

恢复MySQL
```
#仅支持用本工具备份
#配置好程序的配置文件后，执行程序，输入 3/4 即可恢复
#如果没有指定--recover-dir= 或者 -rdir=，程序会根据配置文件配置的备份目录，读取最新的一份备份文件进行恢复。
#恢复的实例是根据配置文件配置的mysql_recover_port，MySQL配置文件也是根据配置文件中定义的配置文件（会将端口改成对应需要恢复的端口）
root ~ >> gfdmp 

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       Management platform info for MySQL
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         Version: V_3.0
         Author : MoShan
         MySQL  : 5.6 or 5.7
         QQ     : 1005155691
         Mail   : mo_shan@yeah.net

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         MySQL current version : 5.7
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
1.  Backup full data for MySQL
2.  Backup increment data for MySQL
3.  Recover data to MySQL
4.  Recover data to MySQL and Create Replication
5.  Initialization profile
6.  Install MySQL
7.  Uninstall MySQL
8.  Install MySQL and Create MySQL Group Replication
9.  Install MySQL and Create Replication
10. EXIT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Please ENTER your choice [1-10]:

gfdmp /root > 3
```

安装MySQL

```
root /data/mysql/20190904 >> gfdmp -idir=/data/test/mysql -V=5.7 -P=9000 -p=123456789 -t=install

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Install info for MySQL on 172.28.85.43
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      MySQL Version     : 5.7
      MySQL port        : 9000
      MySQL password    : 123456789
      MySQL install dir : /data/test/mysql
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-09-04 11:57:58] [INFO] [172.28.85.43] Starting install MySQL...
[2019-09-04 11:57:58] [INFO] [172.28.85.43] Decompressing MySQL installation package...
[2019-09-04 11:58:18] [INFO] [172.28.85.43] Decompression successful...
[2019-09-04 11:58:18] [INFO] [172.28.85.43] Initializing for MySQL...
[2019-09-04 11:58:29] [INFO] [172.28.85.43] Initialization successful for MySQL...
[2019-09-04 11:58:29] [INFO] [172.28.85.43] Starting MySQL...
[2019-09-04 11:58:30] [INFO] [172.28.85.43] Successful startup for MySQL
[2019-09-04 11:58:30] [INFO] [172.28.85.43] This old password root@localhost:J7+jeP&KSCdB
[2019-09-04 11:58:30] [INFO] [172.28.85.43] Changing password root@localhost...
[2019-09-04 11:58:40] [INFO] [172.28.85.43] Change password successfully for root@localhost
[2019-09-04 11:58:40] [INFO] [172.28.85.43] The installation is complete for MySQL 5.7!
[2019-09-04 11:58:40] [INFO] [172.28.85.43] This new password root@localhost:123456789


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
172.28.85.43:Operation command information...
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Connect to MySQL : /data/test/mysql/base/bin/mysql -uroot -p"123456789" -S /data/test/mysql/data/9000/mysqld.sock

Shutdown to MySQL: /data/test/mysql/base/bin/mysqladmin -uroot -p"123456789" -S /data/test/mysql/data/9000/mysqld.sock shutdown

Start to MySQL   : bash /data/test/mysql/mysqld_9000

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


root /data/mysql/20190904 >> 



+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       Management platform info for MySQL
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         Version: V_3.0
         Author : MoShan
         MySQL  : 5.6 or 5.7
         QQ     : 1005155691
         Mail   : mo_shan@yeah.net

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         MySQL current version : 5.7
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
1.  Backup full data for MySQL
2.  Backup increment data for MySQL
3.  Recover data to MySQL
4.  Recover data to MySQL and Create Replication
5.  Initialization profile
6.  Install MySQL
7.  Uninstall MySQL
8.  Install MySQL and Create MySQL Group Replication
9.  Install MySQL and Create Replication
10. EXIT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Please ENTER your choice [1-10]:

gfdmp /root > 6

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Install info for MySQL on 172.28.85.43
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      MySQL Version     : 5.7
      MySQL port        : 9298
      MySQL password    : 8697186051567568910
      MySQL install dir : /data/mysql/20190904
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-09-04 11:48:34] [INFO] [172.28.85.43] Starting install MySQL...
[2019-09-04 11:48:34] [INFO] [172.28.85.43] Decompressing MySQL installation package...
[2019-09-04 11:48:57] [INFO] [172.28.85.43] Decompression successful...
[2019-09-04 11:48:57] [INFO] [172.28.85.43] Initializing for MySQL...
[2019-09-04 11:49:09] [INFO] [172.28.85.43] Initialization successful for MySQL...
[2019-09-04 11:49:09] [INFO] [172.28.85.43] Starting MySQL...
[2019-09-04 11:49:10] [INFO] [172.28.85.43] Successful startup for MySQL
[2019-09-04 11:49:10] [INFO] [172.28.85.43] This old password root@localhost:)fa2_aORAukx
[2019-09-04 11:49:10] [INFO] [172.28.85.43] Changing password root@localhost...
[2019-09-04 11:49:20] [INFO] [172.28.85.43] Change password successfully for root@localhost
[2019-09-04 11:49:20] [INFO] [172.28.85.43] The installation is complete for MySQL 5.7!
[2019-09-04 11:49:20] [INFO] [172.28.85.43] This new password root@localhost:8697186051567568910


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
172.28.85.43:Operation command information...
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Connect to MySQL : /data/mysql/20190904/base/bin/mysql -uroot -p"8697186051567568910" -S /data/mysql/20190904/data/9298/mysqld.sock

Shutdown to MySQL: /data/mysql/20190904/base/bin/mysqladmin -uroot -p"8697186051567568910" -S /data/mysql/20190904/data/9298/mysqld.sock shutdown

Start to MySQL   : bash /data/mysql/20190904/mysqld_9298

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

gfdmp /data/mysql/20190904/base > 
```


卸载MySQL


```
root /data/mysql/20190904 >> gfdmp -idir=/data/test/mysql -V=5.7 -P=9000 -p=123456789 -t=uninstall

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Uninstall info for MySQL on 172.28.85.43
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      MySQL Version     : 5.7
      MySQL port        : 9000
      MySQL password    : 123456789
      MySQL install dir : /data/test/mysql
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-09-04 12:03:56] [INFO] [172.28.85.43] Uninstalling for MySQL...
[2019-09-04 12:03:56] [INFO] [172.28.85.43] Uninstallation completed for MySQL 5.7!

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

root /data/mysql/20190904 >> 

