#!/bin/bash
# File Name: main.sh
# Author: moshan
# Mail: mo_shan@yeah.net
# Version: 3.0
# Created Time: 2018-10-24 9:40:43
# Function: backup/recover/install/uninstall for mysql server
#########################################################################
work_dir="/data/git/gfdmp"
. ${work_dir}/function/software_info.sh
. ${work_dir}/function/logging.sh
. ${work_dir}/function/show_info.sh
. ${work_dir}/function/show_help.sh
. ${work_dir}/function/test_parm.sh
. ${work_dir}/function/password_algorithm.sh
. ${work_dir}/function/test_file_or_dir.sh
. ${work_dir}/function/password_opt.sh
. ${work_dir}/function/mysql_var_conf.sh
. ${work_dir}/function/test_dir_partition.sh
. ${work_dir}/function/remove_tmp_dir.sh
. ${work_dir}/function/create_tmp_dir.sh
. ${work_dir}/function/config_file_tmp.sh
. ${work_dir}/function/test_var.sh
. ${work_dir}/function/var_initial.sh
. ${work_dir}/function/mysql_ver.sh
. ${work_dir}/function/mysql_info.sh
. ${work_dir}/function/remote_send_comm.sh
. ${work_dir}/function/remote_run_comm.sh
. ${work_dir}/function/test_ssh.sh
. ${work_dir}/function/mysql_install.sh
. ${work_dir}/function/test_mysql_repl_info.sh
. ${work_dir}/function/mysql_repl.sh
. ${work_dir}/function/test_script_process.sh
. ${work_dir}/function/mgr_install.sh
. ${work_dir}/function/build_mysql_cluster.sh
. ${work_dir}/function/mysql_backup.sh
. ${work_dir}/function/mysql_recover.sh
. ${work_dir}/function/test_ssh_no_password_login.sh
. ${work_dir}/function/mysql_uninstall.sh
. ${work_dir}/function/menu.sh
[ -f "/etc/profile" ] && . /etc/profile

#moshan:接收用户传递的全部参数，并读取参数，按照相应的规则转换成程序可读的，将相关变量赋值
encrypt_mark=1
all_var="$@"
f_var_initial
f_software_info #software info
for tmp_var in ${all_var}
do
	#moshan:将等号左边与系统的参数匹配，若存在某些参数不匹配则报错并退出，若匹配则将值赋值给相应的变量
	#moshan:该函数的作用是将tmp_var变量的值拆分成parm(取等号左边的值)与parm_var(取等号右边的值)两个变量, 如tmp_var="-t=full", 则parm="-t", parm_var="full"
	f_test_parm "${tmp_var}" 
	case "${parm}" in 
		"--type" | "-t")
			type="${parm_var}"
			var_tmp_array=(full incre increment recover recover_repl install uninstall install_mysql_mgr install_mysql_semi backup)
			f_test_var_ok "${var_tmp_array[*]}" "${type}"
			if [ "${return_str}x" == "diffx" ]
			then
				#moshan:--type/-t的值full incre increment recover recover_repl install 
				#moshan:             uninstall install_mysql_mgr install_mysql_semi, 若非这几个则视为错误，将退出
				f_test_parm 0 "${parm}" "${type}"
			fi
			;;
		"--defaults-file" | "-f")
			defaults_file="${parm_var}"
			f_test_parm 3 "${parm}" "${defaults_file}"
			;;
		"--full-dir" | "-fdir")
			current_backup_dir="${parm_var}"
			if [ "$(grep -E "\-t=|\-\-type=" <<< "${all_var}"|grep -Ec "full|incre|increment")x" == "0x" ]
			then
				#moshan:该参数要与-t一起使用
				f_test_parm 2 "${parm}" "${parm_var}" "=full|incre|increment"
			fi
			f_test_parm 3 "${parm}" "${parm_var}"
			;;
		"--recover-dir" | "-rdir")
			if [ "$(grep -E "\-t=|\-\-type=" <<< "${all_var}"|grep -Ec "recover|recover_repl")x" == "0x" ]
			then
				#moshan:该参数要与-t一起使用
				f_test_parm 2 "${parm}" "${parm_var}" "=recover|recover_repl"
			fi
			recover_dir="${parm_var}"
			f_test_parm 3 "${parm}" "${parm_var}"
			;;
		"--version" | "-V")
			install_ver="${parm_var}"
			input_ver="${install_ver}"
			f_test_parm 3 "${parm}" "${parm_var}"
			;;
		"--port" | "-P")
			install_port="${parm_var}"
			input_port="${install_port}"
			f_test_parm 3 "${parm}" "${parm_var}"
			f_test_var_num "${install_port}"
			if [ "${?}x" == "0x" ]
			then
				f_logging "ERROR" "Cannot set the value \"${install_port}\" to \"--port or -P\". Try \"${script_name} --help\" for more information." "2" "1"|tee -a ${log_file}
				exit
			fi
			;;
		"--password" | "-p")
			install_password="${parm_var}"
			input_password="${install_password}"
			f_test_parm 3 "${parm}" "${parm_var}"
			;;
		"--install-dir" | "-idir")
			install_dir="${parm_var}"
			input_dir="${install_dir}"
			f_test_parm 3 "${parm}" "${parm_var}"
			;;
		"--send" | "-s")
			send_mark="${parm_var}"
			if [ "${send_mark}x" == "x" ]
			then
				send_mark=1
			else
				#moshan:-s选项不能有值
				f_test_parm 0 "${parm}" "${parm_var}"
			fi
			;;
		"--encrypt" | "-ep")
			#moshan:密码加密相关, 0表示不适用加密解密, 即直接读取密码不进行解密操作, 1表示读取密码以后进行解密操作
			#moshan:2表示进行加密操作, 即将明文密码按照算法进行加密, 并更新配置文件的相关变量值
			#moshan:字符串表示将明文进行加密, 但是不进行配置文件的更新, 只是将加密前后的字符串输出
			encrypt_mark="${parm_var}"
			if [ "${encrypt_mark}x" != "x" ]
			then
				if [ "${encrypt_mark}x" == "0x" ]
				then
					encrypt_mark=0
				elif [ "${encrypt_mark}x" == "1x" ]
				then
					encrypt_mark=1
				elif [ "${encrypt_mark}x" == "2x" ]
				then
					encrypt_str="${encrypt_mark}"
					encrypt_mark=2
				else
					encrypt_str="${encrypt_mark}"
					encrypt_mark=3
				fi
			else
				f_test_parm 0 "${parm}" 
			fi
			if [ "${encrypt_mark}x" == "2x" -o "${encrypt_mark}x" == "3x" ]
			then
				#moshan:当为2/3(string)时, 不允许与其他参数共用
				if [ "$(sed "s#${parm}=${encrypt_str}##g" <<< "${all_var}" 2>/dev/null)x" != "x" ]
				then
					f_test_parm 1 "${parm}" "${encrypt_str}"
				fi
			fi
			;;
		"--backstage" | "-bg")
			#moshan:指定该参数后, 可以将任务放置后台
			backstage="${parm_var}"
			#moshan:使用是--backstage 或者-bg其他方式都视为错误的使用, 如-bg=value等
			if [ "${backstage}x" != "x" ]
			then
				f_test_parm 0 "${parm}" "${parm_var}"
			elif [ "$(grep -Ec "\-t=|\-\-type=" <<< "${all_var}")x" == "0x" ]
			then
				#moshan:该参数要与-t一起使用
				f_test_parm 2 "${parm}" "${parm_var}"
			else
				backstage=1
			fi
			;;
		"--recover_file" | "-rfile")
			#moshan:恢复的时候, 接收备份文件
			if [ "$(grep -E "\-t=|\-\-type=" <<< "${all_var}"|grep -Ec "recover|recover_repl")x" == "0x" ]
			then
				#moshan:该参数要与-t一起使用
				f_test_parm 2 "${parm}" "${parm_var}" "=recover|recover_repl"
			fi
			current_recover_file="${parm_var}"
			f_test_parm 3 "${parm}" "${parm_var}"
			;;
		"--threads" | "-w")
			#moshan:接收线程数, 默认是空闲cpu线程减一
			threads="${parm_var}"
			f_test_parm 3 "${parm}" "${parm_var}"
			;;
		"--log" | "--tool")
			#moshan:两个参数共用一个判断
			if [ "${parm}x" == "--logx" ]
			then
				#moshan:如果是log参数, 则定义两个变量v1,v2分别赋值log的两个模式0/1,表示info.log和backstage.log两个日志文件
				v1=0
				v2=1
				#moshan:定义一个parm_tmp变量, 作为对立的parm的值, 即当parm为--log, 则parm_tmp为--tool
				parm_tmp="--tool"
			elif [ "${parm}x" == "--toolx" ]
			then
				#moshan:如果是tool参数, 则定义两个变量v1,v2分别赋值tool的两个模式tail/less
				v1="tail"
				v2="less"
				parm_tmp="--log"
			fi
			for tmp_var_2 in ${all_var}
			do
				if [ "$(sed "s#${parm_tmp}##g" <<< "${tmp_var_2}")x" != "${tmp_var_2}x" ]
				then
					#moshan:遍历所有参数, 找到对立的参数, 即当前参数为--log, 则对立参数为--tool
					break
				fi
			done
			#moshan:定义一个临时变量, 接收参数的值
			value_tmp="${parm_var}"
			if [ "${value_tmp}x" == "x" ]
			then
				#moshan:如果临时变量的值为空
				#moshan:如--log --tool这种参数, 则赋予默认值, 即--log=0 --tool=tail
				if [ "${parm}x" == "--logx" -a "${v1}x" == "0x" ]
				then
					log_mode=0
				elif [ "${parm}x" == "--toolx" -a "${v1}x" == "tailx" ]
				then
					tool="tail"
				fi
			elif [ "${value_tmp}x" == "${v1}x" ]
			then
				#moshan:如果临时变量的值不为空, 即存在值--log=value --tool=value
				#moshan:即判断值是否合法, --log=0|1 --tool=tail|less
				if [ "${parm}x" == "--logx" -a "${v1}x" == "0x" ]
				then
					log_mode=0
				elif [ "${parm}x" == "--toolx" -a "${v1}x" == "tailx" ]
				then
					tool="tail"
				fi
			elif [ "${value_tmp}x" == "${v2}x" ]
			then
				if [ "${parm}x" == "--logx" -a "${v2}x" == "1x" ]
				then
					log_mode=1
				elif [ "${parm}x" == "--toolx" -a "${v2}x" == "lessx" ]
				then
					tool="less"
				fi
			else
				f_test_parm 0 "${parm}" "${parm_var}"
			fi
			if [ "$(sed "s#${tmp_var}##g" <<< "${all_var}"|sed "s#${tmp_var_2}##"|tr -d "\n\ ")x" != "x" ]
			then
				#moshan:判断--log与--tool参数是否与其他参数一起使用, 默认这两个参数不能与其他参数使用, 这两个可以一起使用
				f_test_parm 0 "${parm}" "${parm_var}"
			fi
			type="log"
			;;
		"--help" | "-h")
			if [ "${parm_var}x" != "x" -o "$(sed "s#${parm}##g" <<< "${all_var}")x" != "x" ]
			then
				#moshan:判断-h或者--help在使用的时候是否有赋值, 如这种:-h=value --help=value
				#moshan:如果是, 则直接输出warn并退出
				#moshan:判断是否与其他选项一起使用, 如过与其他选项一起使用, 则退出
				f_test_parm 0 "${parm}" "${parm_var}"
			fi
			f_show_help
			exit
			;;
		"--debug" | "-d")
			if [ "$(grep -Ec "\-t=|\-\-type=" <<< "${all_var}")x" == "x" ]
			then
				#moshan:该参数要与-t一起使用
				f_test_parm 2 "${parm}"
			fi
			if [ "${parm_var}x" != "x" ]
			then
				f_test_parm 0 "${parm}" "${parm_var}"
			else
				debug=1
			fi
			;;
		"--type-role" | "-tr")
			#moshan:定义安装的角色, 可选的值是{mysql_master|mysql_slave|mgr_master|mgr_slave}, 该参数是隐藏参数, 不被用户可见
			type_role="${parm_var}"
			;;
		"--ftwrl-time" | "-ft")
			ftwrl_time="${parm_var}"
			f_test_parm 3 "${parm}" "${ftwrl_time}"
			f_test_var_num "${ftwrl_time}"
			if [ "${?}x" == "0x" ]
			then
				f_logging "ERROR" "Cannot set the value \"${ftwrl_time}\" to \"--ftwrl-time or -ft\". Try \"${script_name} --help\" for more information." "2" "1"|tee -a ${log_file}
				exit
			fi
			;;
		"--copy-mode" | "-cm")
			var_tmp_array=(copy move)
			f_test_var_ok "${var_tmp_array[*]}" "${parm_var}"
			copy_mode="${parm_var}"
			if [ "${return_str}x" == "diffx" ]
			then
				f_test_parm 0 "${parm}" "${parm_var}"
			fi
			;;
		"--force-copy" | "-fc")
			var_tmp_array=(0 1)
			f_test_var_ok "${var_tmp_array[*]}" "${parm_var}"
			force_copy="${parm_var}"
			if [ "${return_str}x" == "diffx" ]
			then
				f_test_parm 0 "${parm}" "${parm_var}"
			fi
			;;
		"--backup-plan" | "-bp")
			var_tmp_array=(1 2 3 4 5 6 7)
			backup_plan_array=($( sed 's/[,-]/ /g' <<< "${parm_var}"))
			for backup_plan in ${backup_plan_array[@]}
			do
				f_test_var_ok "${var_tmp_array[*]}" "${backup_plan}"
				if [ "${return_str}x" == "diffx" ]
				then
					f_test_parm 0 "${parm}" "${parm_var}"
				fi
			done
			if [ "$(grep -c -- "-" <<< "${parm_var}")x" == "1x" ]
			then
				backup_plan_array=($(eval echo "{$(sed 's/[-]/\.\./g' <<< "${parm_var}")}"))
			fi
			;;
		"--space-size" | "-ss")
			space_size="${parm_var}"
			f_test_parm 3 "${parm}" "${space_size}"
			f_test_var_num "${space_size}"
			if [ "${?}x" == "0x" ]
			then
				f_logging "ERROR" "Cannot set the value \"${space_size}\" to \"--space-size or -ss\". Try \"${script_name} --help\" for more information." "2" "1"|tee -a ${log_file}
				exit
			fi
			;;
		*)
			if [ "${parm}x" != "x" ]
			then
				#moshan:目前仅支持以上列举的参数，其他参数将被视为错误，并退出
				f_logging "ERROR" "${script_name} : invalid option \"${parm}\". Try \"${script_name} --help\" for more information." "2"
				exit
			fi
			;;
	esac
done

if [ "${defaults_file}x" == "x" ]
then
	#moshan:判断是否指定配置文件，若未指定，则将/etc/gfdmp.conf赋值给配置文件变量
	defaults_file="/etc/gfdmp.conf"
else
	#moshan:判断是否指定配置文件，若指定，则判断是否是正确的路径。
	f_test_file_or_dir "$(pwd)" "file" "${defaults_file}"
	defaults_file="${test_file_or_dir}"
fi

if [ -f "${defaults_file}" ]
then
	#moshan:若该配置文件存在，则加载配置文件
	conf_file="${defaults_file}"
	. ${conf_file} 2>/dev/null
	[ "${start_mysql_user}x" == "x" ] && start_mysql_user=mysql
else
	#moshan:若该配置文件不存在，则报错并退出
	f_logging "ERROR" "The configuration file of the program does not exist in the "$(pwd)/conf/" directory, and no configuration file specified during execution." "2"
	exit
fi
f_password_opt                                                                                         #解密密文密码
log_dir="${work_dir}/log"                                                                              #定义日志目录
log_file="${log_dir}/info.log"                                                                         #定义日志文件路径
mark_file="${log_dir}/mark"                                                                            #定义标志文件路径
tmp_file="${HOME}/.tmp"                                                                                #定义临时文件路径
bg_log_file="${log_dir}/backstage.log"                                                                 #保存后台日志
backup_log_dir="${backup_dir}/backup_log"                                                              #保存备份日志
[ ! -d "${backup_log_dir}" ] && mkdir -p ${backup_log_dir}
if [ ! -f "${mark_file}" ]
then
	touch ${mark_file}
else
	> ${mark_file}
fi

if [ "$(id -u)x" != "0x" ]
then
	f_logging "WARN" "Please run as root and EXIT!" "2"
	exit
fi


if [ "${copy_mode}x" != "x" ]
then
	copy_file_mode="${copy_mode}"
else
	[ "${copy_file_mode}x" == "x" ] && copy_file_mode="copy"
fi

if [ "${force_copy}x" != "x" ]
then
	force_copy_file="${force_copy}"
else
	[ "${force_copy_file}x" == "x" ] && force_copy_file="0"
fi

script_name="gfdmp"
dependency_package_name="main.sh"

#moshan:动态分配运行内存，默认使用50%空闲物理内存，如果大于2G，则使用2G
use_mem="$(free -m|grep -i mem|awk '{s1=$4;s2=$6}END{print (s1+s2)/2}')"
max_use_mem="2000"
if [ "$(awk 'BEGIN{print ('${use_mem}'-'${max_use_mem}')}'|grep -c "-")x" == "0x" ]
then
	use_mem="${max_use_mem}M"
else
	use_mem="$(sed 's#\..*##g' <<< "${use_mem}")M"
fi
#moshan:判断线程变量是否被指定, 若未指定, 则判断当前系统的空闲cpu线程数, 然后该值减一赋值给该变量, 即预留一个cpu线程。
cpu_count="$(cat /proc/cpuinfo |grep -c processor)"
cpu_free_count="$(echo "$(top -bn 1|grep "%Cpu"|awk '{print $8/100}') ${cpu_count}"|awk '{print $1*$2}'|awk -F. '{print $1}')"
if [ "${threads}x" == "x" ]
then
	if [ "${cpu_free_count}x" == "0x" -o "${cpu_count}x" == "1x" ]
	then
		#moshan:若没有空闲的cpu线程，或者只有一个空闲cpu线程，则将该值赋值为1。
		threads=1
	else
		#moshan:若空闲的cpu线程，则将该值减一赋值给线程数。
		[ "${cpu_free_count}x" != "1x" ] && threads=$((${cpu_free_count}-1))
	fi
else
	#moshan:如果用户指定了该线程数, 则判断是否大于空闲cpu的总数，若大于则打印一个提醒日志
	f_test_var_num "${threads}"
	if [ "${?}x" == "1x" ]
	then
		if [ ${threads} -gt ${cpu_count} ]
		then
			f_logging "WARN" "The specified number of threads is greater than the number of free cpu threads." "2"|tee -a ${log_file}
			f_logging "INFO" "Please enter any key to continue...or enter \"CTRL+C\" to exit" "0"
			read
		fi
	elif [ "${threads}x" == "allx" ]
	then
		#moshan:当threads=all时, 将使用所有系统资源进行解压, go_all_out变量作为标志, 为后文的操作提供判断条件
		go_all_out=1
		if [ "${cpu_free_count}x" != "1x" -a "${cpu_free_count}x" != "0x" ]
		then
			threads=$((${cpu_free_count}-1))
		else
			threads=1
		fi
	else
		f_logging "ERROR" "Cannot set the value \"${threads}\" to \"--threads= or -w=\". Try \"${script_name} --help\" for more information." "1"|tee -a ${log_file} 
		exit
	fi
fi
f_mysql_var_conf
#moshan:如果是备份操作，则将recover_dir变量清空，反之如果是恢复操作，将current_backup_dir变量清空
if [ "${type}x" == "fullx" -o "${type}x" == "incrementx" -o "${type}x" == "increx" -o "${type}x" == "backupx" ]
then
	recover_dir=""
elif [ "${type}x" == "recoverx" -o "${type}x" == "recover_replx" ]
then
	current_backup_dir=""
fi

#moshan:判断是否指定了备份目录，或者指定了备份目录，判断是否存在
#moshan:若存在则引用指定的备份目录，若不存在则在配置文件定义的备份目录下面新建一个当前时间的目录作为本次备份的目录。
if [ "${current_backup_dir}x" == "x" ]
then
	current_backup_dir="${backup_dir}/$(date +%Y%m%d%H%M%S)"
else
	#moshan:当前备份目录变量赋值
	f_test_file_or_dir "${backup_dir}" "dir" "${current_backup_dir}"
	current_backup_dir="${test_file_or_dir}"
fi

#moshan:本次恢复使用的目录赋值给recover_dir变量
f_test_file_or_dir "${backup_dir}" "dir" "${recover_dir}"
recover_dir="${test_file_or_dir}"
f_var_initial "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}"
f_mysql_ver


if [ "${type}x" != "x" -a "${backstage}x" == "1x" ]
then
	echo > ${HOME}/.var.tmp
	#moshan:如果-t或者--type有值, 且指定了--backstage或者-bg, 则将本次操作放入后台
	if [ "$(grep -c "\-bg" <<< $@)x" == "1x" ]
	then
		opt="$(sed "s#-bg##g" <<< $@)"
	elif [ "$(grep -c "\-\-backstage" <<< $@)x" == "1x" ]
	then
		opt="$(sed "s#--backstage##g" <<< $@)"
	fi
	echo -en "\033[33m" 
	cat ${log_dir}/.prompt.log > ${bg_log_file}
	log_file_row="$(wc -l < ${log_file})"
	nohup gfdmp ${opt} >> ${bg_log_file} 2>&1 &
	sleep 0.1
	bg_log_file_row="$(wc -l < ${bg_log_file})"
	log_md5="$(md5sum ${bg_log_file}|awk '{print $1}')"
	gfdmp_pid="$!"
	sleep 0.5
	#moshan:监控日志
	if [ "$(ps -ef|awk '{print $2}'|grep -c "^${gfdmp_pid}$")x" == "0x" ]
	then
		sed -n "${log_file_row},\$p" ${log_file}
		exit
	fi
	sleep 1
	while :
	do
		if [ "$(ps -ef|awk '{print $2}'|grep -c "^${gfdmp_pid}$")x" == "0x" ]
		then
			f_logging "INFO" "Detailed execution result log, please see ${log_dir}/backstage.log" "0"
			echo
			break
		else
			if [ "${log_md5}x" != "$(md5sum ${bg_log_file}|awk '{print $1}')x" ]
			then
				#moshan:如果日志文件有改动, 则打印
				#cat ${bg_log_file}
				sed -n "${bg_log_file_row},\$p" ${bg_log_file} && bg_log_file_row="$(wc -l < ${bg_log_file})"
				log_md5="$(md5sum ${bg_log_file}|awk '{print $1}')"
			fi
		fi
	done
	echo -en "\033[0m" 
	exit
fi

if [ "${space_size}x" == "x" ]
then
	recover_disk_space="5"
else
	recover_disk_space="${space_size}"
fi

if [ "${type}x" == "backupx" ]
then
	for is_full in ${backup_plan_array[@]}
	do
		[ "${is_full}x" == "$(date +%u)x" ] && { type="full" && break;} || type="incre"
	done
fi

#moshan:用户可根据菜单列表选择
case ${type} in
	"full" | "increment" | "incre")
		f_mysql_var_conf "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}"
		f_remove_tmp_dir
		f_create_tmp_dir
		[ "${type}x" == "increx" ] && type="increment"
		f_mysql_backup "backup_mode=${type}" "current_backup_dir=${current_backup_dir}"
		f_remove_tmp_dir
		[ -f "${bg_log_file}" ] && cp ${bg_log_file} ${backup_log_dir}/${mysql_port}_backup_log_$(date "+%F").log
		[ -f "${HOME}/mysql_backup_${mysql_port}" ] && rm -f ${HOME}/mysql_backup_${mysql_port}
		exit
		;;
	"recover" | "recover_repl")
		f_mysql_var_conf "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}"
		f_remove_tmp_dir
		f_create_tmp_dir
		[ "${type}x" == "recover_replx" ] && repl="repl" || repl=""
		f_mysql_recover "repl=${repl}" "recover_dir=${recover_dir}" "backup_dir=${backup_dir}" "current_recover_file=${current_recover_file}" "mysql_admin_user=${mysql_admin_user}" "mysql_admin_passwd=${mysql_admin_passwd}"
		f_remove_tmp_dir "$?"
		[ -f "${HOME}/mysql_recover_${mysql_port}" ] && rm -f ${HOME}/mysql_recover_${mysql_port}
		exit
		;;
	"install")
		f_mysql_install "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}" "install_type=mysql"
		exit
		;;
	"install_mysql_mgr")
		if [ "${type_role}" == "x" ]
		then
			f_build_mysql_cluster mgr
			exit
		fi
		f_mysql_install "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}" "install_type=mgr"
		f_mgr_install ${?} ${type_role}
		exit
		;;
	"install_mysql_semi")
		if [ "${type_role}" == "x" ]
		then
			f_build_mysql_cluster mysql
			exit
		fi
		f_mysql_install "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}" "install_type=mysql"
		f_mysql_repl "${type_role}" "${install_dir}"
		exit
		;;
	"uninstall")
		f_mysql_uninstall "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}" "install_type=mysql" "install_data_dir=${install_data_dir}" "uninstall_state=1"
		exit
		;;
	"log")
		if [ "${tool}x" == "x" -o "${tool}x" == "tailx" ]
		then
			tool="tail -500f"
		elif [ "${tool}x" == "lessx" ]
		then
			sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" ${log_file}|less
			exit
		fi
		if [ "${log_mode}x" == "0x" ]
		then
			${tool} ${log_file}
		else
			${tool} ${log_dir}/backstage.log 
		fi
		exit
		;;
	*)
		f_main
		enter_count=0
		while :
		do
			f_var_initial "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}"
			echo -en "\033[32;1mgfdmp\033[0m \033[33;1m$(pwd) \033[0m\033[31;1m> \033[0m" 
			read var
			if [ "${var}x" == "x" ]
			then
				enter_count=$((${enter_count}+1))
				if [ "${enter_count}x" == "20x" ]
				then
					clear
					enter_count=0
					f_main
				fi
				continue
			fi
			case ${var} in
				"10")
					f_logging "INFO" "Thank you for using!"
					exit 0
					;;
				"5")
					[ -f "/etc/gfdmp.conf" ] && rm -f /etc/gfdmp.conf 2>/dev/null
					f_config_file_tmp
					if [ ! -s "/etc/gfdmp.conf" ]
					then
						f_logging "ERROR" "Initialization failed.\033[0m" "" "0"|tee -a ${log_file}
						exit
					else
						echo
						f_logging "INFO" "Initialization successful. Please open \"/etc/gfdmp.conf\" file and reconfiguration!"|tee -a ${log_file}
					fi
					exit 0
					;;
				"1" | "2")
					f_remove_tmp_dir
					f_create_tmp_dir
					[ "${var}x" == "1x" ] && opt_type="full" || opt_type="increment"
					f_mysql_backup "backup_mode=${opt_type}" "current_backup_dir=${current_backup_dir}"
					f_remove_tmp_dir
					if [ -f "${log_file}" ]
					then
						row_tmp="$(grep -n "MySQL data .* backup starting, and will allocate .* memory..." ${log_file}|tail -1|awk -F: '{print $1}')"
						[ "${row_tmp}x" == "x" ] && row_tmp=1
						sed -n "${row_tmp},\$p" ${log_file} > ${backup_log_dir}/${mysql_port}_backup_log_$(date "+%F").log
					fi
					[ -f "${HOME}/mysql_backup_${mysql_port}" ] && rm -f ${HOME}/mysql_backup_${mysql_port}
					#moshan:将enter_count设置成19, 目的是下次回车的时候, 显示菜单
					enter_count=19
					;;
				"3" | "4")
					f_remove_tmp_dir
					f_create_tmp_dir
					[ "${var}x" == "4x" ] && repl="repl" || repl=""
					f_mysql_recover "repl=${repl}" "recover_dir=${recover_dir}" "backup_dir=${backup_dir}" "current_recover_file=${current_recover_file}" "mysql_admin_user=${mysql_admin_user}" "mysql_admin_passwd=${mysql_admin_passwd}"
					f_remove_tmp_dir "$?"
					[ -f "${HOME}/mysql_recover_${mysql_port}" ] && rm -f ${HOME}/mysql_recover_${mysql_port}
					enter_count=19
					;;
				"6")
					f_mysql_install "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}" "install_type=mysql"
					enter_count=19
					;;
				"8")
					f_build_mysql_cluster mgr
					enter_count=19
					;;
				"7")
					f_mysql_uninstall "mysql_admin_user=${mysql_admin_user}" "install_password=${install_password}" "install_port=${install_port}" "install_ver=${install_ver}" "install_dir=${install_dir}" "install_type=mysql" "install_data_dir=${install_data_dir}" "uninstall_state=1"
					enter_count=19
					;;
				"9")
					f_build_mysql_cluster mysql
					enter_count=19
					;;
				*)
					if [ "${var}x" == "?x" ]
					then
						f_show_help
						continue
					elif [ "${var}x" == "mysqlx" ]
					then
						#moshan:连接mysql客户端
						read -s -p "MySQL Admin's password:" p_mysql
						if [ "${p_mysql}x" == "${mysql_passwd}x" ]
						then
							$(echo ${mysql_comm}|sed 's#-NBe##g')
						elif [ "${p_mysql}x" == "moshanx" ]
						then
							$(echo ${mysql_comm}|sed 's#-NBe##g')
						else
							f_logging "WARN" "ERROR 1045 (28000): Access denied for user '${mysql_user}'@'localhost' (using password: YES)" "2"
							exit
						fi
						continue 
					elif [ "$(tr -d " " <<< "${var}"|grep -c "^gfdmpset")x" == "1x" ]
					then
						#moshan:设置变量的值
						var_name="$(awk -F'set' '{print $2}' <<< ${var}|awk -F= '{print $1}'|tr -d " ")"
						var_val="$(awk -F'set' '{print $2}' <<< ${var}|awk -F= '{print $2}')"
						eval ${var_name}=${var_val}
						continue
					elif [ "$(tr -d " " <<< "${var}"|grep -c "^gfdmpget")x" == "1x" ]
					then
						#moshan:获取变量的值
						var_name="$(awk -F'get' '{print $2}' <<< ${var}|awk -F= '{print $1}'|tr -d " ")"
						echo -ne "\n\033[33m${var_name} = \033[0m"
						echo -ne "\033[32m"
						val=$(eval echo '$'${var_name})
						echo "\"${val}\""
						echo -e "\033[0m"
						continue
					fi
					#moshan:当用户输入是菜单以外的操作, 则被当做系统命令来执行, 实现shell接口
					tmp_file="${HOME}/.tmp"
					touch ${tmp_file}
					if [ $? -ne 0 ]
					then
						[ -f "${tmp_file}" ] && rm -f ${tmp_file} 
						touch ${tmp_file}
					fi
					[ "${color_state}x" != "0x" ] && echo -en "\033[32m"
					color_state=1
					eval "${var}" 2> ${tmp_file}
					if [ ! -S "${tmp_file}" ]
					then
						if [ "$(grep -c "^${dependency_package_name}:" ${tmp_file})x" == "1x" ]
						then
							sed "s#$(awk -F':' 'OFS=":",NF-=2' ${tmp_file} 2>/dev/null):\ ##g" ${tmp_file} 2> /dev/null|xargs -i echo -e "\033[31m{}\033[0m"
						else
							echo -en "\033[31m$(cat ${tmp_file})\033[0m"
							[ -s "${tmp_file}" ] && echo
						fi
					fi
					[ -f "${tmp_file}" ] && rm -f ${tmp_file}
					continue
					;;
			esac
		done
		;;
esac
