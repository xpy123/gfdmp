work_dir="/data/git/gfdmp"
. ${work_dir}/function/show_info.sh
function f_show_help()
{
	#moshan: 帮助信息函数
	echo
	f_show_info
	echo -e "\033[32m -----------------------------------------------\033[0m"
	echo -e "\033[33m"
	echo "Usage: ${script_name} [OPTION]..."
	echo
	echo "--type=value or -t=value          The value=full | increment or incre | recover | recover_repl | install | uninstall | install_mysql_mgr | install_mysql_semi | backup | null"
	echo "                                  For example: --type=full,-t=full,--type=increment,-t=increment,--type=incre,-t=incre,"
	echo "                                               --type=recover,-t=recover,--type=recover_repl,-t=recover_repl"
	echo "                                               --type=install,-t=install,--type=uninstall,-t=uninstall"
	echo "                                  If it is uninstalled, please specify --password or -p, --install-dir or -idir, --port or -P."
	echo "                                  The default value is empty. The value is empty and show menu."
	echo
	echo "--defaults-file= or -f=           Read default options from the given file."
	echo "                                  For example: --defaults-file=/gfdmp_installation_path/conf/gfdmp.conf,-f=/gfdmp_installation_path/conf/gfdmp.conf"
	echo "                                               --defaults-file=gfdmp.conf,-f=gfdmp.conf"
	echo "                                  The default value is \"/etc/gfdmp.conf\""
	echo
	echo "--full-dir= or -fdir=             Specify a directory of the full file."
	echo "                                  For example: --full-dir=/mysql_backup_dir/$(date +%s),-fdir=/mysql_backup_dir/$(date +%s)"
	echo "                                  AND if the input is a relative path, it will be automatically modified to an absolute path."
	echo "                                  For example:\"$(date +%s)\" equivalent to \"/mysql_backup_dir/$(date +%s)\""
	echo "                                  The default value is the latest backup file in /mysql_backup_dir/$(date +%s)"
	echo
	echo "--recover-dir= or -rdir=          Specify a directory of the backup file. [ full backup or full backup and increment backup ]."
	echo "                                  For example: --recover-dir=/mysql_backup_dir/$(date +%s),-rdir=/mysql_backup_dir/$(date +%s)"
	echo "                                  AND if the input is a relative path, it will be automatically modified to an absolute path."
	echo "                                  For example:\"$(date +%s)\" equivalent to \"/mysql_backup_dir/$(date +%s)\""
	echo "                                  The default value is the latest backup file in /mysql_backup_dir/$(date +%s)"
	echo
	echo "--recover-file= or -rfile=        Specify a file of the full backup file."
	echo "                                  For example: --recover-file=/mysql_backup_dir/$(date +%s)/full_backup_file_name"
	echo "                                               --recover-file=full_backup_file_name"
	echo "                                               -rfile=/mysql_backup_dir/$(date +%s)/full_backup_file_name"
	echo "                                               -rfile=full_backup_file_name"
	echo "                                  The default value is the latest backup file in /mysql_backup_dir/$(date +%s)"
	echo
	echo "--password= or -p=                Specify a password of the admin, effective during installation or uninstallation"
	echo "                                  For example: --password=12345678, -p=12345678"
	echo "                                  The default value is an Nine-digit random number. For example: --password=$(date +%N)"
	echo
	echo "--port= or -P=                    Specify an unused port for MySQL, effective during installation"
	echo "                                  For example: --port=3306, -P=3306. The default value is 9298."
	echo
	echo "--version= or -V=                 Specify a version for MySQL, and this version of installation package exist in the \"/gfdmp_installation_dir/components\" dir,"
	echo "                                  effective during installation. For example: \"--version=5.6\", \"-V=5.7\". The default value is \"5.7\"."
	echo
	echo "--install-dir= or -idir=          Specify a dir of the MySQL installation dir, effective during installation or uninstallation"
	echo "                                  If it is installed, please specify an empty dir or a dir that does not exist."
	echo "                                  If it is uninstalled, please specify MySQL installation dir."
	echo "                                  For example: --install-dir=/data/mysql/, -idir=/data/mysql"
	echo "                                  AND if the input is a relative path, it will be automatically modified to an absolute path."
	echo "                                  For example:\"install_dir\" equivalent to \"/data/mysql/install_dir\""
	echo "                                  The default value is \"/data/mysql/$(date +%F|tr -d "-")\""
	echo
	echo "--threads= or -w=                 Decompress/compress the number of concurrent. For example:--threads=8"
	echo "                                  The default value is the number of free cpu minus one."
	echo
	echo "--encrypt= or -ep=                Encrypt the password of the mysql administrator and mysql backup user in the configuration file \"/etc/gfdmp.conf\"."
	echo "                                  The value=\"0 | 1 | 2 | string\". The default value is \"1\"."
	echo "                                  No encryption algorithm is used, and the password in the configuration file is an unencrypted string when value=0"
	echo "                                  Use an encryption algorithm, and the password in the configuration file is an encrypted string when value=1"
	echo "                                  Cannot be used with other options when value=\"2 | string\""
	echo "                                  Convert the string to an encrypted string and output the encrypted string when value=\"string\"."
	echo
	echo "--log=                            Print gfdmp run log. The value=\"0 | 1\", the default value is \"0\", and \"--log\" is equivalent to \"--log=0\"."
	echo "                                  When value=0 shows the log of the info.log file, value=1 shows the log of the backstage.log file."
	echo
	echo "--tool=                           Choose tool for displaying \"info.log | backstage.log\" logs."
	echo "                                  The value=\"less | tail\", the default value is \"tail\", and \"--tool\" is equivalent to \"--tool=tail\"."
	echo
	echo "--ftwrl-time= or -ft=             This option specifies time in seconds that innobackupex should wait for queries that would block FTWRL before running it."
	echo "                                  If there are still such queries when the timeout expires, innobackupex terminates with an error. "
	echo "                                  Default is 0, in which case innobackupex does not wait for queries to complete and starts FTWRL immediately."
	echo
	echo "--copy-mode= or -cm=              Copy data file mode, support copy and move. Useful only when doing data recovery. The value=\"copy | move\"."
	echo "                                  The default value is \"copy\", when copy is specified and the data directory is not empty, the data directory will not be replaced."
	echo "                                  If forced replacement is required, you can specify --force-copy=1 or -fc=1"
	echo
	echo "--force-copy= or -fc=             Whether to force copy data files when the data directory is not empty. Useful only when doing data recovery."
	echo "                                  The value=0 | 1, the default value is 0."
	echo
	echo "--backup-plan= or -bp=            Full backup plan for MySQL. Only valid when --type=backup or -t=backup. The value=1,5 | 1-5"
	echo "                                  1,5  : Full backup on Monday, Friday and incremental backup on Tuesday, Wednesday, Thursday, Saturday, Sunday."
	echo "                                  1-5  : FUll backup on Monday to Friday and incremental backup on Saturday, Sunday."
	echo
	echo "--space-size= or -ss=             Free disk space size requirement is greater than \"backup_file_size\" * \"this_value\"."
	echo "                                  Only valid when --type=recover or -t=recover. The default value is 5. Please set greater than or equal to 5."
	echo
	echo
	echo
	echo -e "\033[32mThe following parameters do not require a value.\033[33m"
	echo
	echo "--backstage or -bg                Put this operation in the background to execute. Use with \"-t or --type\" option"
	echo "                                  For example:\"-t=full -bg\". The default value is off."
	echo
	echo "--debug or -d                     Debug mode, and print backup or recovery commands."
	echo
	echo "--send or -s                      Sending backup file to remote server. For example: --send,-s"
	echo
	echo "--help or -h                      Display this help and exit."
	echo -e "\033[0m"
	echo
}

