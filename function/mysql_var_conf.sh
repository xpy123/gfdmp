work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_mysql_var_conf()
{
	#moshan:初始化mysql相关的变量
	if [ ! -f "${mysql_conf}" ]
	then
		#moshan: 除了备份恢复等四个操作, 对于其他操作不予以判断mysql配置文件是否存在
		if [ "$(grep -cE "^full$|^increment$|^incre$|^recover$|^recover_repl$" <<< "${type}")x" == "1x" ]
		then
			f_logging "ERROR" "MySQL configuration file \"${mysql_conf}\" does not exist and EXIT" "2" "1"|tee -a ${log_file} 
		else
			return
		fi
	fi
	mysql_sock="$(tr -d " \t" < ${mysql_conf}|sed 's#^ ##g'|grep -v "^#"|grep -i "^socket="|tail -1|awk -F= '{print $2}')"
	mysql_data_dir="$(tr -d " \t" < ${mysql_conf}|sed 's#^ ##g'|grep -v "^#"|grep -i "^datadir="|awk -F= '{print $2}')"
	mysql_base_dir="$(tr -d " \t" < ${mysql_conf}|sed 's#^ ##g'|grep -v "^#"|grep -i "^basedir="|awk -F= '{print $2}')"
	mysql_error_file="$(tr -d " \t" < ${mysql_conf}|sed 's#^ ##g'|grep -v "^#"|grep -i "^log[-_]error="|awk -F= '{print $2}')"
	mysql_binlog_dir="$(tr -d " \t" < ${mysql_conf}|sed 's#^ ##g'|grep -v "^#"|grep -i "^log[_-]bin="|awk -F= '{print $2}'|awk -F'/' 'OFS="/",NF-=1')"
	mysql_port="$(tr -d " \t" < ${mysql_conf}|sed 's#^ ##g'|grep -v "^#"|grep -i "^port=.*[0-9]$"|tail -1|awk -F= '{print $2}')"
	mysql_full_backup_file="${mysql_port}_full_backup_file_$(date +%Y%m%d%H%M%S)"
	mysql_increm_backup_file="${mysql_port}_incremental_backup_file_for"
	[ "$(grep -c ${mysql_port} <<< "${backup_dir}x" 2> /dev/null)x" == "0x" ] && backup_dir="${backup_dir}/${mysql_port}"
	backup_tmp="${backup_dir}/tmp"                                                                         #定义备份所用的临时目录集
	backup_tmp_dir1="${backup_tmp}/tmp1"                                                                   #定义备份所用的临时目录1
	backup_tmp_dir2="${backup_tmp}/tmp2"                                                                   #定义备份所用的临时目录2
	backup_tmp_dir3="${backup_tmp}/tmp3"                                                                   #定义备份所用的临时目录3
	backup_tmp_dir4="${backup_tmp}/tmp4"                                                                   #定义备份所用的临时目录4
	mysql_comm="${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p${mysql_admin_passwd} -S ${mysql_sock} -NBe"       #定义mysql的连接命令，通过sock连接
	master_gtid_info_file="${work_dir}/.master_gtid_info.log"                                              #保存master的gtid信息
}

