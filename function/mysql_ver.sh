work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_mysql_ver()
{
	#moshan:判断mysql的版本
	mysql_ver_tmp="$(${mysql_base_dir}/bin/mysql -V 2>/dev/null|grep -c 5.6)"
	if [ "${mysql_ver_tmp}x" == "1x" ]
	then
		mysql_ver="5.6"
	else
		mysql_ver="5.7"
	fi

	export PATH=${work_dir}/bin/${mysql_ver}:${PATH}
	#moshan:判断是否存在xtrabackup环境
	xtraback_exists="$(which xtrabackup 2>/dev/null|wc -l)"
	if [ "${xtraback_exists}x" == "0x" ]
	then
		f_logging "INFO" "Check the \"xtrabackup\" environment for this OS"
		f_logging "WARN" "The system has no \"xtrabackup\" environment, and preparing the environment, please wait..."|tee -a ${log_file}
		#moshan:如果不存在, 则安装
		tar_file="percona-xtrabackup-2.4.9-Linux-x86_64.tar.gz"
		ping -c 1 baidu.com > /dev/null 2>&1
		if [ $? -ne 0 ]
		then
			f_logging "WARN" "Preparation failed because the connection to the network failed..."|tee -a ${log_file}
		else
			f_logging "INFO" "Preparing to download \"xtrabackup\"..." "1"|tee -a ${log_file}
			wget https://www.percona.com/downloads/XtraBackup/Percona-XtraBackup-2.4.9/binary/tarball/${tar_file} >/dev/null 2>&1 &
			wget_pid=$!
			sleep 1
			while :
			do
				#moshan:检查下载进程
				if [ "$(ps -ef|awk '{print $2}'|grep -c "^${wget_pid}$")x" == "1x" ]
				then
					sleep 3
					echo -n ".."|tee -a ${log_file}
				else
					if [ -f "${tar_file}" ]
					then
						f_logging "INFO" "Download completed."|tee -a ${log_file}
						f_logging "INFO" "Start installation"|tee -a ${log_file}
					else
						f_logging "WARN" "Download failed."|tee -a ${log_file}
					fi
					break
				fi
			done
			tar_tmp_dir="${work_dir}/bin/tmp/"
			mkdir -p ${tar_tmp_dir} 2>/dev/null
			tar zxf ${tar_file} -C ${tar_tmp_dir} 2>/dev/null
			if [ $? -ne 0 ]
			then
				f_logging "WARN" "The installation failed."|tee -a ${log_file}
			else
				#moshan:拷贝二进制文件到相应的目录
				cp ${tar_tmp_dir}/percona-xtrabackup-2.4.9-Linux-x86_64/bin/* ${work_dir}/bin/5.7
				if [ $? -eq 0 ]
				then
					f_logging "INFO" "The installation is complete."|tee -a ${log_file}
				else
					f_logging "ERROR" "The installation failed." "2" "0"|tee -a ${log_file}
					[ -d "${tar_tmp_dir}" ] && rm -rf ${tar_tmp_dir}
					return
				fi
			fi
		fi
	fi
	xtrabackup_path="$(which xtrabackup 2>/dev/null)"
	xtrabackup_dir="$(awk -F'/' 'OFS="/",NF-=1' <<< "${xtrabackup_path}")"
	innobackupex_path="${xtrabackup_dir}/innobackupex"
	if [ -f "${xtrabackup_path}" -a ! -f "${innobackupex_path}" ]
	then
		ln -s ${xtrabackup_path} ${innobackupex_path} >/dev/null 2>&1
	fi
}

