work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
. ${work_dir}/function/mysql_info.sh
function f_mysql_uninstall()
{
	mysql_admin_user="root"
	install_password="$(awk -F= '{print $2}' <<< "${2}")"
	install_port="$(awk -F= '{print $2}' <<< "${3}")"
	install_ver="$(awk -F= '{print $2}' <<< "${4}")"
	install_dir="$(awk -F= '{print $2}' <<< "${5}")"
	install_type="$(awk -F= '{print $2}' <<< "${6}")"
	install_data_dir="$(awk -F= '{print $2}' <<< "${7}")"
	uninstall_state="$(awk -F= '{print $2}' <<< "${8}")"
	install_sock_file="${install_data_dir}/mysqld.sock"
	if [ "${uninstall_state}x" == "0x"  ]
	then
		#moshan:卸载的时候, 如果没有指定相应的变量的值, 则终止卸载操作, 返回主菜单
		install_dir="${input_dir}"
		install_ver="${input_ver}"
		install_password="${input_password}"
		install_port="${input_port}"
		f_mysql_info Uninstall
		f_logging "ERROR" "There is an error in the uninstall information...." "2" "0"|tee -a ${log_file}
		return
	else
		f_mysql_info Uninstall
	fi
	echo -e "\033[32m"
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo
	f_logging "INFO" "Uninstalling for MySQL..."|tee -a ${log_file}
	#echo "${mysql_base_dir}/bin/mysqladmin -u${mysql_admin_user} -p\"${install_password}\" -S ${install_sock_file} shutdown >/dev/null 2>&1"
	${mysql_base_dir}/bin/mysqladmin -u${mysql_admin_user} -p"${install_password}" -S ${install_sock_file} shutdown >/dev/null 2>&1
	if [ $? -eq 0 ]
	then
		#moshan:下载的思路是直接停掉mysqld进程, 然后删掉mysql安装目录
		#moshan:仅限卸载通过本程序安装的mysql
		chattr -R -i ${install_dir} > /dev/null 2>&1
		[ -d "${install_dir}" ] && rm -rf ${install_dir}/* 2>/dev/null 
		rmdir ${install_dir}
		[ $? -ne 0 ] && [ -d "${install_dir}" ] && rm -rf ${install_dir} 2>/dev/null
		echo
		f_logging "INFO" "Uninstallation completed for MySQL ${install_ver}!"|tee -a ${log_file}
		mysql_tmp_pid="$(netstat -anplt|grep mysqld|grep ":${install_port} "|awk '{print $NF}'|awk -F/ '{print $1}')"
		if [ "${mysql_tmp_pid}x" != "x" ]
		then
			#moshan: 卸载的时候可能出现一种情况, shutdown成功执行后, mysqld进程并未退出或者又被拉起, 所以使用kill -9暴力杀掉
			mysql_tmp_pid="$(ps -ef|awk '{print $2" "$3}'|grep "^${mysql_tmp_pid} ")"
			kill -9 ${mysql_tmp_pid}
		fi
	else
		f_logging "ERROR" "Uninstall failed for MySQL ${install_ver}, Please check the uninstall information..." "1"|tee -a ${log_file}
	fi
	echo -e "\033[32m"
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo -e "\033[0m"
}

