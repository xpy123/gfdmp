function f_mysql_info()
{
	echo -e "\033[32m"
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "      ${1} info for MySQL on ${localhost_ip}" 
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "      MySQL Version     : ${install_ver}"
	echo "      MySQL port        : ${install_port}"
	echo "      MySQL password    : ${install_password}"
	echo "      MySQL install dir : ${install_dir}"
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo -e "\033[0m"
}

