function f_config_file_tmp()
{
	#moshan:初始化配置文件函数
    cat <<EOF > /etc/gfdmp.conf
work_dir="/data/git/gfdmp"
backup_dir="/tmp/backup"                                         #定义备份目录 
start_mysql_user="mysql"                                         #启动mysql服务的用户
mysql_conf="/data/mysql/etc/3311/my.cnf"                         #mysql的备份节点的配置文件路径
mysql_recover_port="3312"                                        #mysql的恢复节点的端口，默认使用备份节点的配置文件，仅将相关端口改成该值
mysql_admin_user="test"                                          #mysql的管理员用户
mysql_admin_passwd="999ea00f2e2447461462462e2611100"             #mysql管理员用户对应的密码, 请配置成明文, 然后带-ep=2选项执行即可生成密文[密码中不能包含如下几个特殊符号"!#&=", 及其他特殊字符, 尽量使用大小写+数字] 
mysql_backup_user="test"                                         #mysql备份用户，也可以直接用管理员用户跟密码
mysql_backup_passwd="7cae10e32447461462466649b7c"                #mysql管理员用户对应的密码, 请配置成明文, 然后带-ep=2选项执行即可生成密文[密码中不能包含如下几个特殊符号"!#&=" , 及其他特殊字符, 尽量使用大小写+数字]



repl_info=(172.28.85.43 repl repl 3311 % slave)                  #建立复制的主机的信息，如果需要与某master建立复制关系则需要配置。如下六行注释很重要！！！
                                                                 #针对最后一个值，ip + port一定要正确，如果是slave要配成备份节点的ip+port
                                                                 #如果是master要配成备份节点的master的ip+port。
                                                                 #repl_info数组的最后一个值：
                                                                 #slave ：表示与备份节点建立主从关系
																 #master：表示与备份节点的master建立主从关系，前提是备份时指定了相应的备份参数
																 #判断依据是恢复的数据目录需要存在xtrabackup_slave_info文件。




backup_count=3                                                   #至多保留几份全备文件
recover_disk_space=5                                             #恢复所需磁盘空间是备份文件的倍数，建议大于等于5, 即剩余磁盘空间建议大于备份文件5倍
force_full_backup=1                                              #当增量备份的时候在全量备份目录中的全量备份文件不可用时，则自动做一份全量备份，0表示关闭该功能
remote_host_port=22                                              #ssh进程端口
remote_host_user=root                                            #如果有需要将备份文件放置远端服务器的需求可以配上该值，但要求该用户对源端到目标端拥有免密登陆
backup_host=(10.186.19.83 10.186.19.84)                          #ip列表空格分开，当备份完成后将会根据需求将备份文件发送至相应的ip，可作为从库恢复用，或者转储
mysql_cluster_host=(10.186.19.83 10.186.19.84)                   #ip列表空格分开，当备份完成后将会根据需求将备份文件发送至相应的ip，可作为从库恢复用，或者转储
send_backup_file_force=0                                         #发送备份文件前会先通过ping来检查一下网络，如果网络禁用icmp的话可以将该值设为1，否则建议是0
mysql_data_dir_filesystem="/dev/mapper/mint--vg-root"            #mysql数据目录所在的分区文件系统，通过df查看后取第一列的值
background_operation=1                                           #如果想把恢复操作放入后台, 此时指定的mysql配置文件中的端口与mysql_recover_port一致的情况下,该参数必须为1, 否则将退出操作
backup_encrypt=1                                                 #备份文件是否加密, 1表示加密, 0表示不加密
remove_qp_file=1                                                 #qp文件是否删除, 1表示删除, 0表示不删除
copy_file_mode="move"                                            #恢复的时候拷贝数据文件的模式，支持copy和move的方式
force_copy_file=0                                                #拷贝数据文件时, 如果数据目录非空则不进行拷贝, 如果该选项为1则继续拷贝, 默认是0
encrypt_passwd="8sK7cKHs05a8Ts7sHd0675cIKa7KsMad"                #备份文件加密密码，是有当backup_encrypt=1才有效

#请注意：mysql配置文件中要存在mysql sock文件的配置，mysql 安装目录的配置，mysql 数据目录的配置，mysql 端口的配置
EOF
}

