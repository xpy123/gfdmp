work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_test_file_or_dir()
{
	#moshan:判断一个文件或者目录是否存在，若不存在则退出。
	#moshan:如果存在则格式化其路径，如果是绝对路基，则原样输出，如果是相对路径则加上对应的全路径
	find_dir="${1}"
	test_type="${2}"
	test_file_or_dir="${3}"
	[ "${test_file_or_dir}x" == "x" ] && return
	if [ "$(grep "^\./" <<< "${test_file_or_dir}"|awk -F'\\./' '{print $NF}')x" != "x" ]
	then
		#moshan:如果是通过"./"这种方式的相对路径，则将"./"格式化去掉，如./test变成test
		test_file_or_dir="$(awk -F'\\./' '{print $NF}' <<< "${test_file_or_dir}")"
	fi
	if [ "$(grep -v "^/" <<< "${test_file_or_dir}"|grep -c "/")x" != "0x" ]
	then
		#moshan:目前只支持最后一层相对路径，即test或./test这种，而不支持test/test这种，如果是这种则抛出错误并退出
		f_logging "ERROR" "There are wrong format on \"${test_file_or_dir}\" and EXIT..." "2" "1"
	fi
	if [ "${test_type}x" == "filex" ]
	then
		find_type="f"
	elif [ "${test_type}x" == "dirx" ]
	then
		find_type="d"
	fi
	test_file_or_dir="$(find ${find_dir} -type ${find_type} -name "${test_file_or_dir}" 2>/dev/null)"
	if [ "${test_file_or_dir}x" == "x" ]
	then
		#moshan:先进行一次判断，看看是否能找到，若找不到则原值赋值，等待下面的处理
		test_file_or_dir="${3}"
	fi
	if [ "$(awk -F'/' '{print $1}' <<< "${test_file_or_dir}")x" != "x" ]
	then
		#moshan:判断是否是绝对路径，如果不是则进行下面的判断
		if [ "$(awk -F'/' '{print $NF}' <<< "${test_file_or_dir}")x" == "x" ]
		then
			#moshan:判断是否该相对路径是以"/"结尾，如果是则去掉最后的"/"
			test_file_or_dir="$(echo ${test_file_or_dir}|sed 's#.$##g')"
		fi
		show_dir="$(pwd)"
		for((i=0;i<3;i++))
		do
			#moshan:如果传过来的是file，则对其进行文件的判断，如果是dir，则对其进行目录的判断。
			if [ ! -${find_type} "${test_file_or_dir}" ]
			then
				if [ "${i}x" == "0x" ]
				then
					#moshan:第一次查询的处理，如果当前查找的目录与要显示的目录相同（第一次是在当前目录下），则跳过后面的日志提醒
					[ "${show_dir}x" == "${find_dir}x" ] && continue
					#moshan:先在当前目录下搜索，若搜不到则到提供的目录搜索
					f_logging "WARN" "The ${test_type} \"${test_file_or_dir}\" was not found in the \"${show_dir}\" directory..."
					f_logging "WARN" "Now, try to find the ${test_type} \"${test_file_or_dir}\" in the \"${find_dir}\" directory..."
				elif [ "${i}x" == "1x" ]
				then
					#moshan:第二次查找，若第一次查找子啊当前目录找不到，则第二次查到/etc下面找
					f_logging "ERROR" "The ${test_type} \"${test_file_or_dir}\" was not found in the \"${show_dir}\" directory and EXIT..." "2" "1"
					#moshan:若找不到则原值赋值，等待下面的处理
					test_file_or_dir="${3}"
					if [ "${test_type}x" == "dirx" ]
					then
						#moshan:若经过一次查找，并未找到结果，如果查找类型是目录，则直接退出程序
						exit
					else
						#moshan:如果查找的类型是文件，第一次查找未找到，则将查找目录改为/etc，再次查找
						find_dir="/etc"
						f_logging "WARN" "Now, try to find the ${test_type} \"${test_file_or_dir}\" in the \"${find_dir}\" directory..."
					fi
				else
					#moshan:当在当前目录查找不到，且也不能在/etc下查不到，则退出系统
					test_file_or_dir="${3}"
					f_logging "ERROR" "The ${test_type} \"${test_file_or_dir}\" was not found in the \"${show_dir}\" directory and EXIT..." "2" "1"
				fi
				#moshan:重新查找，如果是查找文件的话，查找目录是/etc
				test_file_or_dir="$(find ${find_dir} -type ${find_type} -name "${test_file_or_dir}" 2>/dev/null)"
				if [ "${test_file_or_dir}x" == "x"  ]
				then
					#moshan:如果还是没有找到文件，则原值赋值，并交由后面处理。
					test_file_or_dir="${3}"
				fi
				#moshan:显示的目录改成本次查找的目录，即/etc
				show_dir="${find_dir}" 
			else
				#moshan:如果存在，且是第一次循环，则将其加上全路径，因为进到if条件里面的都是相对路径的。
				if [ "${i}x" == "0x" ]
				then
					test_file_or_dir="$(pwd)/${test_file_or_dir}"
				fi
			fi
		done
	else
		#moshan:如果是绝对路径，则直接返回该文件或目录。
		test_file_or_dir="${test_file_or_dir}"
		if [ ! -${find_type} "${test_file_or_dir}" ]
		then
			f_logging "ERROR" "The ${test_type} \"${test_file_or_dir}\" was not found in the OS and EXIT..." "2" "1"
		fi
	fi
}
