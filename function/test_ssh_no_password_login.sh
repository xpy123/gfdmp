work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_test_ssh_no_password_login()
{
	echo -e "\033[33m"
	for ((i=0;i<${#mysql_cluster_host[@]};i++))
	do
		state_file="$(date|md5sum|awk '{print $1}')"
		> ${state_file}
		ssh -p ${remote_host_port} ${remote_host_user}@${mysql_cluster_host[${i}]} "echo 1" > ${state_file} 2>/dev/null &
		state_comm_pid="${!}"
		for ((time_out=0;time_out<5;time_out++))
		do
			if [ "$(cat ${state_file})x" == "1x" ]
			then
				ssh_state=1
				break
			elif [ "${time_out}x" == "4x" ]
			then
				ssh_state=0
				state_comm_exists="$(ps -ef|grep -v grep|awk '{print $2}'|grep -c "^${state_comm_pid}$")"
				if [ "${state_comm_exists}x" == "1x" ]
				then
					kill -9 ${state_comm_pid} > /dev/null 2>&1
					sleep 1
				fi
			else
				sleep 1
			fi
		done
		#[ -f "${state_file}" ] && rm -f ${state_file}
		if [ "${ssh_state}x" == "0x" ]
		then
			f_logging "WARN" "The server \"${localhost_ip}\" to \"${mysql_cluster_host[${i}]}\" are not configured with public key login"|tee -a ${log_file}
			f_logging "INFO" "Start configuring public key login for server \"${localhost_ip}\" to \"${mysql_cluster_host[${i}]}\""|tee -a ${log_file}
			if [ -f "~/.ssh/id_rsa.pub" ]
			then
				cat ~/.ssh/id_rsa.pub|ssh -p ${remote_host_port} ${remote_host_user}@${mysql_cluster_host[${i}]} "cat >> ~/.ssh/authorized_keys" 2> ${state_file}
				if [ $? -ne 0 ]
				then
					printf "\033[1;31;40m\033[0m\n"
				fi

			fi
		fi
	done
}

