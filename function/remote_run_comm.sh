work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_remote_run_comm()
{
	remote_host_ip="${1}"
	remote_run_comm_tmp="${2}"
	if [ "${remote_run_comm_tmp}x" == "psx" ]
	then
		#moshan: process_count作为返回值, 1表示进程还在, 0表示进程执行完毕
		process_count=$(ssh -p ${remote_host_port} ${remote_host_user}@${remote_host_ip} "ps -ef|grep 'bin/${dependency_package_name}'|grep bash|awk '{print \$9}'|grep -c 'bin/${dependency_package_name}\$'" 2>/dev/null)
		echo "[${localhost_ip}] ps -ef|grep 'bin/${dependency_package_name}'|grep bash|awk '{print \$9}'|grep -c 'bin/${dependency_package_name}\$'" > ${log_file}
		unset remote_host_ip remote_run_comm_tmp
		return ${process_count}
	elif [ "${remote_run_comm_tmp}x" == "catx" ]
	then
		#moshan: mark_state作为返回值
		mark_state=$(ssh -p ${remote_host_port} ${remote_host_user}@${remote_host_ip} "cat ${mark_file}" 2>/dev/null)
		unset remote_host_ip remote_run_comm_tmp
		return ${mark_state}
	else
		ssh -p ${remote_host_port} ${remote_host_user}@${remote_host_ip} "export PATH=${work_dir}/bin:\${PATH};${remote_run_comm_tmp}" 2>/dev/null
		if [ "${?}x" != "0x" ]
		then
			f_logging "WARN" "Failed to execute remote command [COMM:${remote_run_comm_tmp}] for ${remote_host_ip}." "2" >> ${log_file}
		fi
		unset remote_host_ip remote_run_comm_tmp
	fi
}

