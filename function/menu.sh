work_dir="/data/git/gfdmp"
. ${work_dir}/function/show_info.sh
function f_main()
{
	f_show_info
	echo -ne "\033[32m"
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "         MySQL current version : ${mysql_ver}"
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "1.  Backup full data for MySQL"
	echo "2.  Backup increment data for MySQL"
	echo "3.  Recover data to MySQL"
	echo "4.  Recover data to MySQL and Create Replication"
	echo "5.  Initialization profile"
	echo "6.  Install MySQL"
	echo "7.  Uninstall MySQL"
	echo "8.  Install MySQL and Create MySQL Group Replication"
	echo "9.  Install MySQL and Create Replication"
	echo "10. EXIT"
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "Please ENTER your choice [1-10]:"
	echo -e "\033[0m"

}

