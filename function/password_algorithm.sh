work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_password_algorithm()
{
	#moshan: 密码加密解密算法函数
	opt_mode="${1}"
	str_tmp="${2}"
	output=""
	result=""
	if [ "${opt_mode}x" == "encryptx" ]
	then
		#moshan:字符串加密, 每个字符转换成ascii, 并加上316, 如果加密后的长度大于10, 则交换字符串的收尾
		#moshan:将后尾的十个字符放置在首部, 如果不大于10个字符, 则原样输出
		for ((i=0;i<${#str_tmp};i++));do
			#获取输入每一个字符并转换为ASCII码
			letter=$(printf "%d" "'${str_tmp:$i:1}")
			letter=$((${letter}+346))
			result+=${letter}
		done
		if [ ${#result} -lt 10 ];then
			output=${result}
		else
			output=${result:$((${#result}-10)):${#result}}${result:0:$((${#result}-10))}
		fi
		#moshan:最后随机生成一个随机数, 并生成一个随机字符串, 字符串长度与随机数等长
		#moshan:在加密后的密码串的首部加上刚才生成的随机数及随机字符串, 如原来加密后的密码串是123456789abcd, 生成的一个随机数是3
		#moshan:根据随机数3生成一个三位的随机数234, 则最终的密码串是3234123456789abcd234
		#moshan:即第一位是随机数, 然后随机数后面的是随机字符串, 然后最后尾也是随机字符串
		tmp="$(tr -d "0" <<< $(date +%N))"
		tmp="${tmp:2:1}"
		count="$(date +%N|md5sum)"
		output="${tmp}$(echo ${count:1:${tmp}})${output}${count:15:${tmp}}"
	elif [ "${opt_mode}x" == "decryptx" ]
	then
		#moshan:将需要解密的字符串, 先取第一个值, 以此可知首尾有几位是随机数, 比如总长度是29, 随机数是2
		#随机数是2, 所以可知, 首部两个字符与尾部两个字符是随机数
		tmp="${str_tmp:0:1}"
		#moshan:将首部的随机数位数及对应的随机数个数去掉, 即仅保留从第3个字符开始往后的字符串
		str_tmp="${str_tmp:$((${tmp}+1))}"
		#moshan:经过上一步骤, 总长度还剩26
		#moshan:将首个字符开始截取到第24个字符(26-2)
		str_tmp="${str_tmp:0:$((${#str_tmp}-${tmp}))}"
		#moshan:经过上一步骤, 总长度还剩24个字符
		#moshan:因为加密过程中, 还将收尾移位了, 下面步骤是将位置移回来
		#moshan:从第11位开始截取到最后, 加上从第一位开始截取到第10位字符就是加密后的完整字符串
		str_tmp="${str_tmp:10:$((${#str_tmp}-10))}${str_tmp:0:10}"
		if [ "$((${#str_tmp}%3))x" == "0x" ]
		then
			#moshan:将字符串按每三位对应一个字符, 并将acsii码转化为具体的字符, 并拼接成完整的字符串.
			for ((i=0;i<${#str_tmp};i=i+3))
			do
				output+=$(printf \\x$(printf %x $((${str_tmp:${i}:3}-346))))
			done
		fi
		if [ "${output}x" == "x" ]
		then
			#moshan:如果解密的结果是空字符串, 则可能是因为原字符串是明文
			f_logging "ERROR" "\"${tmp}${str_tmp}\" may be an unencrypted password. Decryption failed and EXIT." "2" "1"
		fi
	fi

}

