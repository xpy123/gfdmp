function f_test_dir_partition()
{
	#moshan:测试某路径的分区
	test_dir="$1"
	if [ "$(awk -F/ '{print $NF}' <<< "${test_dir}")x" == "x" ]
	then
		#moshan:如果是以/结尾的, 则去掉尾部的/
		test_dir="$(awk -F'/' 'OFS="/",NF-=1' <<< "${test_dir}")"
	fi
	#moshan:判断传过来的路径的深度, 如/test/test/test, 表示深度为3, 根据深度循环
	dir_depth="$(grep -o "/" <<< "${test_dir}"|grep -c "/")"
	for ((i=0;i<=${dir_depth};i++))
	do
		test_dir_par="$(df|awk '{print $NF}'|grep -c "^${test_dir}$")"
		if [ "${test_dir_par}x" == "0x" ]
		then
			#moshan:如果查找无果, 则将路径去掉最后一层, 如查找/test/test/test无果, 则变为/test/test继续查找
			test_dir="$(awk -F'/' 'OFS="/",NF-=1' <<< "${test_dir}")"
			[ -z "${test_dir}" ] && test_dir="/"
		else
			test_dir_par="$(df|awk '{print $1" "$NF}'|grep " ${test_dir}$"|awk '{print $1}')"
		fi
		#moshan:若都无果, 则返回根分区
	done
}

