work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_test_mysql_repl_info()
{
	#moshan: 检查MySQL复制相关信息, 包括用户名密码, master的主机地址及端口的正确性(检查是否为空而已)
	if [ "${repl_info[2]}x" == "x" -o "${mysql_cluster_host[0]}x" == "x" ]
	then
		#moshan: ip地址及端口不可为空
		f_logging "ERROR" "Sorry, the replication account information you configured is incorrect." "2" "1"
	fi
	if [ "${repl_info[3]}x" == "x" ]
	then
		#moshan: 用户名密码及主机段为空的话, 赋值默认值
		repl_info[3]="$(awk -F. '{print $1"."$2}' <<< "${mysql_cluster_host[0]}")"
		if [ "${repl_info[1]}x" == "x" ]
		then
			repl_info[1]="repl_password"
			if [ "${repl_info[0]}x" == "x" ]
			then
				repl_info[0]="repl_user"
			fi
		fi
	fi
}

