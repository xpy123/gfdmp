work_dir="/data/git/gfdmp"
. ${work_dir}/function/password_algorithm.sh
function f_password_opt()
{
	#moshan: 使用密码算法函数进行加密解密操作
	if [ "${encrypt_mark}x" == "0x" ]
	then
		#moshan:不做任何操作
		sleep 0.001
	elif [ "${encrypt_mark}x" == "1x" ]
	then
		#moshan:将密码字符串读取出来, 并用加密解密算法将字符串解析出来
		f_password_algorithm decrypt "${mysql_admin_passwd}"
		mysql_admin_passwd="${output}"
		f_password_algorithm decrypt "${mysql_backup_passwd}"
		mysql_backup_passwd="${output}"
	elif [ "${encrypt_mark}x" == "2x" ]
	then
		#moshan:根据配置文件的密码配置, 读取出来并进行字符串加密, 并更新配置文件
		f_password_algorithm encrypt "${mysql_admin_passwd}"
		mysql_passwd="${output}"
		f_password_algorithm encrypt "${mysql_backup_passwd}"
		mysql_backup_passwd="${output}"
		cp ${conf_file} ${conf_file}_$(date +%s)
		sed -i "s#^mysql_admin_passwd.*\#\(.*\)#mysql_admin_passwd=\"${mysql_admin_passwd}\"      \#\1#g" ${conf_file}
		sed -i "s#^mysql_backup_passwd.*\#\(.*\)#mysql_backup_passwd=\"${mysql_backup_passwd}\"      \#\1#g" ${conf_file}
	elif [ "${encrypt_mark}x" == "3x" ]
	then
		#moshan:将相关字符串进行加密, 然后显示到终端
		f_password_algorithm encrypt "${encrypt_str}"
		echo
		echo -e "\033[32mBefore encryption : \033[0m\033[33m${encrypt_str}\033[0m"
		echo -e "\033[32mAfter  encryption : \033[0m\033[33m${output}\033[0m"
		echo
		exit
	fi
}

