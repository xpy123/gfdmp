work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
. ${work_dir}/function/remote_run_comm.sh
function f_test_ssh()
{
	#function:f_test_ssh "1" "${mysql_cluster_host[0]}"
	ssh_ok=0
	ssh_count=0
	for ((i=${1};i<${#mysql_cluster_host[@]};i++))
	do
		#moshan: 检查ssh免密码登录是否正常, ssh_count表示主机的计数器, 每检查一次就记录数加一, ssh_ok表示ssh正常计数器, ssh正常就加一
		ssh_count=$((${ssh_count}+1))
		f_remote_run_comm "${mysql_cluster_host[${i}]}" "hostname" >/dev/null
		if [ "${?}x" == "0x" ]
		then
			ssh_ok=$((${ssh_ok}+1))
			f_logging "INFO" "Server ${2} to server ${mysql_cluster_host[${i}]} login normal for ssh public key"|tee -a ${log_file}
		else
			f_logging "WARN" "Server ${2} to server ${mysql_cluster_host[${i}]} login normal for ssh public key" "2"|tee -a ${log_file}
		fi
	done
	if [ "${ssh_ok}x" != "${ssh_count}x" ]
	then
		#moshan: 如果ssh_ok和ssh_count的值不相等, 则表示有ssh异常的主机, 这时候将打印相关日志, 让用户选择是否继续
		f_logging "WARN" "The cluster has some node exceptions for ssh public key"|tee -a ${log_file}
		f_logging "WARN" "Now, you can ENTER [Y|y] to continue this operation, and you can also ENTER other to abandon this operations." "2"|tee -a ${log_file}
		echo -en "\033[33mPlease ENTER your choice within ten seconds [ Y | y ]:\033[0m"|tee -a ${log_file}
		read -t 10 chioce
		if [ "${chioce}x" == "Yx" -o "${chioce}x" == "yx" ]
		then
			f_logging "WARN" "You ENTER [${chioce}] to continue this operations." "2"|tee -a ${log_file}
			return
		else
			if [ "${chioce}x" == "x" ]
			then
				f_logging "WARN" "You did not ENTER any information, the program will automatically skip this operation"|tee -a ${log_file}
			else
				f_logging "WARN" "You ENTER [${chioce}] to abandon this operations." "2"|tee -a ${log_file}
			fi
			return
		fi
	fi
	unset ssh_ok ssh_count chioce
}

