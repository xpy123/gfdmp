work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_test_var()
{
	#moshan:检查相应的变量是否为空，下面变量中，部分变量不可为空，为空的话直接退出系统
	if [ "${mysql_sock}x" == "x" ]
	then
		f_logging "ERROR" "The sock configuration does not exist in the ${mysql_conf} and EXIT!" "2" "1"|tee -a ${log_file}
	fi
	if [ "${mysql_data_dir}x" == "x" ]
	then
		f_logging "ERROR" "The mysql_data_dir configuration does not exist in the ${mysql_conf} and EXIT!" "2" "1"|tee -a ${log_file}
	fi
	if [ "${mysql_port}x" == "x" ]
	then
		f_logging "ERROR" "The mysql port configuration does not exist in the ${mysql_conf}  and EXIT!" "2" "1"|tee -a ${log_file}
	fi
	if [ ! -d "${mysql_base_dir}" ]
	then
		f_logging "ERROR]" "mysql_base_dir is not exist and EXIT!" "2" "1"|tee -a ${log_file}
	fi
	if [ "${mysql_error_file}x" == "${mysql_data_dir}/x" ]
	then
		mysql_error_file="${mysql_data_dir}/$(hostname).err"
	fi
}

