work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_var_initial()
{
	mysql_admin_user="$(awk -F= '{print $2}' <<< "${1}")"
	install_password="$(awk -F= '{print $2}' <<< "${2}")"
	install_port="$(awk -F= '{print $2}' <<< "${3}")"
	install_ver="$(awk -F= '{print $2}' <<< "${4}")"
	install_dir="$(awk -F= '{print $2}' <<< "${5}")"
	#moshan:mysql相关变量初始化, 安装/卸载的时候需要用到
	[ "${install_ver}x" == "x" ] && install_ver="5.7"
	[ "${install_port}x" == "x" ] && install_port="9298" && uninstall_state=0
	[ "${install_password}x" == "x" ] && install_password="$(date "+%N%s")" && uninstall_state=0
	if [ "${install_dir}x" == "x" ] 
	then
		#moshan:若未指定安装路径, 则按如下定义
		uninstall_state=0
		install_dir="/data/mysql/$(date +%F|tr -d "-")"
	elif [ "$(awk -F'/' '{print $1}' <<< "${install_dir}")x" != "x" ]
	then
		#moshan:定义安装目录, 如果不是绝对路径的, 则格式化成绝对路径
		install_dir="/data/mysql/${install_dir}"
	fi
	localhost_ip=($(ip a 2>/dev/null|grep inet|grep "brd.*scope"|awk '{print $2}'|awk -F'/' '{print $1}'))
	for ((i=0;i<${#localhost_ip[@]};i++))
	do
		for ((j=0;j<${#mysql_cluster_host[@]};j++))
		do
			if [ "${localhost_ip[${i}]}x" == "${mysql_cluster_host[${j}]}x" ]
			then
				unset localhost_ip
				localhost_ip=${mysql_cluster_host[${j}]}
				break
			fi
		done
		[ "${#localhost_ip[@]}x" == "1x" ] && break
	done
	[ "${localhost_ip}x" == "x" ] && localhost_ip="127.0.0.1"
	server_id="$(awk -F. '{print $3$4}' <<< "${localhost_ip}")${install_port}"
	CLIENT_WORK_DIR="${work_dir}"
	tmp_dir="${CLIENT_WORK_DIR}/tmp"
	[ ! -d "${tmp_dir}" ] && mkdir -p ${tmp_dir}
	install_base_dir="${install_dir}/base"
	install_data_dir="${install_dir}/data/${install_port}"
	install_conf_file_tmp="${work_dir}/conf/my.cnf.${install_ver}"
	if [ ! -f "${install_conf_file_tmp}" ]
	then
		#moshan:根据用户指定的版本, 判断模板配置文件是否存在, 不存在则退出安装
		f_logging "ERROR" "The file \"${install_conf_file_tmp}\" was not found in ${work_dir}" "2" "0"|tee -a ${log_file}
		return 1
	fi
	install_conf_file="${install_dir}/etc/${install_port}/my.cnf"
	install_sock_file="${install_data_dir}/mysqld.sock"
	install_error_file="${install_data_dir}/mysql-error.log"
	mysql_id_info=($(grep "^mysql:" /etc/passwd|awk -F: '{print $3" "$4}'))
	id ${start_mysql_user} 2>/dev/null|grep "${mysql_id_info[0]}"|grep "${mysql_id_info[1]}(mysql)" > ${tmp_dir}/tmp.log 2>/dev/null
	if [ "0x" == "$(wc -l < ${tmp_dir}/tmp.log)x" ]
	then
		groupadd ${start_mysql_user} 2>/dev/null
		useradd ${start_mysql_user} -g ${start_mysql_user} 2>/dev/null
		usermod -a -G ${start_mysql_user} ${start_mysql_user} 2>/dev/null
	fi
	if [ -f "${tmp_dir}/tmp.log" ]
	then
		rm -f ${tmp_dir}/tmp.log 
		rmdir ${tmp_dir} 2> /dev/null
	fi
	return 0
}
