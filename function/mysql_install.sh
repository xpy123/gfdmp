work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
. ${work_dir}/function/mysql_info.sh
function f_mysql_install()
{
	mysql_admin_user="root"
	install_password="$(awk -F= '{print $2}' <<< "${2}")"
	install_port="$(awk -F= '{print $2}' <<< "${3}")"
	install_ver="$(awk -F= '{print $2}' <<< "${4}")"
	install_dir="$(awk -F= '{print $2}' <<< "${5}")"
	install_type="$(awk -F= '{print $2}' <<< "${6}")"
	echo 0 > ${mark_file}
	if [ $? -eq 1 ]
	then
		return
	fi
	if [ "$(ls ${install_dir} 2> /dev/null|wc -l)x"  != "0x" ]
	then
		f_logging "WARN" "${install_dir} is not empy and EXIT!" "2" "0"|tee -a ${log_file}
		return
	fi
	if [ "$(netstat -anplt |awk '{print $4}'|grep -c ":${install_port}$")x" != "0x" ]
	then
		f_logging "WARN" "The port [${install_port}] has been used and EXIT!" "2"|tee -a ${log_file}
		return
	fi
	mysql_tar_file="$(ls -l ${CLIENT_WORK_DIR}/components/mysql*-${install_ver}* 2> /dev/null|tail -1|awk '{print $NF}')"
	if [ ! -f "${mysql_tar_file}" ]
	then
		f_logging "ERROR" "MySQL installation package of ${install_ver} does not exists in the \"${CLIENT_WORK_DIR}/components\" and EXIT" "2" "0"|tee -a ${log_file}
		unset mysql_tar_file
		return
	fi
	if [ "${install_type}x" == "mgrx" ]
	then
		#moshan: 如果是搭建MGR, 则必须是使用5.7版本的MySQL, 且使用MGR的配置文件
		if [ "${install_ver}x" != "5.7x" ]
		then
			f_logging "ERROR" "If you need to install MGR cluster you must choose MySQL 5.7" "2" "0"
			unset mysql_tar_file
			return
		else
			#moshan: 如果是安装MGR端口号不能大于9999, 因为MGR内部有一个端口通信, 默认是端口号加50000
			if [ "$(grep -c "-" <<< "$(awk '{print $1-9999}' <<< "${install_port}")")x" == "0x" ]
			then
				f_logging "WARN" "The port [${install_port}] number is too large, please use a number less than 9999 as the port number. !" "2"|tee -a ${log_file}
				unset mysql_tar_file
				return
			else
				install_conf_file_tmp="${work_dir}/conf/my.cnf.mgr"
			fi
		fi
	fi
	mkdir -p ${install_dir}/{data/${install_port},binlog/${install_port},tmp/${install_port},etc/${install_port}}
	sed "s#/opt/mysql#${install_dir}#g" ${install_conf_file_tmp} > ${install_conf_file}
	sed -i "s#3306#${install_port}#g" ${install_conf_file}
	sed -i "s#server_id_num#${server_id}#g" ${install_conf_file}
	innodb_buffer_var_tmp="$(sed 's/M//g' <<< "${use_mem}"|awk '{print $0/2"M"}')"
	sed -i "s#innodb_buffer_var#${innodb_buffer_var_tmp}#g" ${install_conf_file}
	sed -i "s#localhost_ip#${localhost_ip}#g;s#mgr_service_port#${install_port}#g;s#mgr_host1_ip#${mysql_cluster_host[0]}#g;s#mgr_host2_ip#${mysql_cluster_host[1]}#g;s#mgr_host3_ip#${mysql_cluster_host[2]}#g;" ${install_conf_file}
	f_mysql_info Install
	f_logging "INFO" "Starting install MySQL..." "1" "1"
	f_logging "INFO" "Decompressing MySQL installation package..."|tee -a ${log_file}
	tar zxf ${mysql_tar_file} -C ${install_dir}
	tmp="$(ls ${install_dir}/mysql*-${install_ver}* -d)"
	if [ -n "${tmp}" ]
	then
		install_base_dir="${install_dir}/base"
		mv ${tmp} ${install_base_dir}
		f_logging "INFO" "Decompression successful..."|tee -a ${log_file}
	else
		f_logging "INFO" "Decompression error..."|tee -a ${log_file}
		unset mysql_tar_file install_conf_file_tmp
		return
	fi
	chown -R ${start_mysql_user}. ${install_dir}
	f_logging "INFO" "Initializing for MySQL..."|tee -a ${log_file}
	if [ "${install_ver}x" == "5.7x" ]
	then
		if [ "$(md5sum ${install_conf_file_tmp}|awk '{print $1}'|grep -cE "6d6b205519e65aa80da01e7fbd161c0f|cde4311c85238f4f15896df2cab186a8")x" == "0x" ]
		then
			f_logging "WARN" "This file \"${install_conf_file_tmp}\" has been changed..." "2"|tee -a ${log_file}
		fi
		cd ${install_base_dir} && ./bin/mysqld --defaults-file=${install_conf_file} --initialize --user=${start_mysql_user} > /dev/null 2>&1
		if [ $? -ne 0 ]
		then
			[ -d "${install_dir}" ] && rm -rf ${install_dir}
			f_logging "ERROR" "Initialization failed for MySQL..." "2" "0"|tee -a ${log_file}
			unset mysql_tar_file install_conf_file_tmp tmp
			return
		else
			f_logging "INFO" "Initialization successful for MySQL..."|tee -a ${log_file}
		fi
		mysql_password_old="$(grep "A temporary password is generated for root@localhost:" ${install_error_file} 2>/dev/null|awk '{print $NF}')"
	elif [ "${install_ver}x" == "5.6x" ]
	then
		if [ "$(md5sum ${install_conf_file_tmp}|awk '{print $1}')x" != "597cc5b7978dceae2e29eb062c9fd25dx" ]
		then
			f_logging "WARN" "This file \"${install_conf_file_tmp}\" has been changed..." "2"|tee -a ${log_file}
		fi
		cd ${install_base_dir} && ./scripts/mysql_install_db --defaults-file=${install_conf_file} --random-passwords --user=${start_mysql_user} > /dev/null 2>&1
		if [ $? -ne 0 ]
		then
			[ -d "${install_dir}" ] && rm -rf ${install_dir}
			f_logging "ERROR" "Initialization failed for MySQL..." "2" "0"|tee -a ${log_file}
			unset mysql_tar_file install_conf_file_tmp tmp mysql_password_old
			return
		else
			f_logging "INFO" "Initialization successful for MySQL..."|tee -a ${log_file}
		fi
		mysql_password_old="$(grep "random password" ~/.mysql_secret 2> /dev/null|tail -1|awk '{print $NF}')"
	fi
	if [ "$(grep -ic error ${install_error_file})x" != "0x" ]
	then
		[ -d "${install_dir}" ] && rm -rf ${install_dir}
		f_logging "ERROR" "Initialization failed for MySQL and EXIT" "2" "0"|tee -a ${log_file}
		unset mysql_tar_file install_conf_file_tmp tmp mysql_password_old
		return
	fi
	f_logging "INFO" "Starting MySQL..."|tee -a ${log_file}
	echo "cd ${install_base_dir} && ./bin/mysqld_safe --defaults-file=${install_conf_file} --user=${start_mysql_user} >/dev/null 2>&1 &" > ${install_dir}/mysqld_${install_port}
	echo exit >> ${install_dir}/mysqld_${install_port}
	sh ${install_dir}/mysqld_${install_port}
	while :
	do
		sleep 1
		if [ "$(ps -ef|grep mysqld_safe|grep -c ${install_conf_file})x" == "0x" ]
		then
			f_logging "ERROR" "Startup failed for MySQL" "2" "0"|tee -a ${log_file}
			return
		fi
		if [ "$(ls ${install_sock_file} 2>/dev/null|wc -l)x" == "1x" ]
		then
			f_logging "INFO" "Successful startup for MySQL"|tee -a ${log_file}
			break
		fi
	done
	f_logging "INFO" "This old password root@localhost:${mysql_password_old}"|tee -a ${log_file}
	f_logging "INFO" "Changing password root@localhost..."|tee -a ${log_file}
	for ((i=0;i<=10;i++))
	do
		if [ "${install_ver}x" == "5.7x" ]
		then
			${install_base_dir}/bin/mysql -u${mysql_admin_user} -p"${mysql_password_old}" -S ${install_sock_file} --connect-expired-password -e "set global super_read_only=0;alter user root@'localhost' identified by '${install_password}';" >/dev/null 2>&1
		else
			${install_base_dir}/bin/mysql -u${mysql_admin_user} -p"${mysql_password_old}" -S ${install_sock_file} --connect-expired-password -e "set password=password('${install_password}');" >/dev/null 2>&1
		fi
		${install_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${install_sock_file} -NBe "select 1;" >/dev/null 2>&1
		if [ $? -eq 0 ]
		then
			f_logging "INFO" "Change password successfully for root@localhost"|tee -a ${log_file}
			f_logging "INFO" "The installation is complete for MySQL ${install_ver}!"|tee -a ${log_file}
			f_logging "INFO" "This new password root@localhost:${install_password}"
			echo > ${tmp_file}
			echo >> ${tmp_file}
			echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
			echo "${localhost_ip}:Operation command information..." >> ${tmp_file}
			echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
			echo >> ${tmp_file}
			echo "Connect to MySQL : ${install_base_dir}/bin/mysql -u${mysql_admin_user} -p\"${install_password}\" -S ${install_sock_file}" >> ${tmp_file}
			echo >> ${tmp_file}
			echo "Shutdown to MySQL: ${install_base_dir}/bin/mysqladmin -u${mysql_admin_user} -p\"${install_password}\" -S ${install_sock_file} shutdown" >> ${tmp_file}
			echo >> ${tmp_file}
			echo "Start to MySQL   : bash ${install_dir}/mysqld_${install_port}" >> ${tmp_file}
			echo >> ${tmp_file}
			echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
			echo >> ${tmp_file}
			echo -ne "\033[33m"
			cat ${tmp_file}
			echo -ne "\033[0m"
			[ -f "${tmp_file}" ] && rm -f ${tmp_file}
			echo 1 > ${mark_file}
			break
		else	
			sleep 1
		fi
		if [ ${i} -eq 10 ]
		then
			f_logging "ERROR" "Failed to change password for root@localhost" "2" "0"|tee -a ${log_file}
			unset mysql_tar_file install_conf_file_tmp tmp mysql_password_old
			return
		fi
		unset mysql_tar_file install_conf_file_tmp tmp mysql_password_old
	done
	return 1
}

