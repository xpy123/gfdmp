work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_test_parm()
{
	#moshan: 判断参数是否正确, 及参数的使用是否正确
	opt_tmp="${1}" #moshan:接收传过来的参数选项加值, 如-t=full
	parm="$(echo ${opt_tmp}|awk -F'=' '{print $1}')" 
	if [ "${opt_tmp}x" == "0x" ]
	then
		#moshan:参数1表示模式, 参数2表示参数, 参数三表示值
		#moshan:当模式为0, 或者值为空都可以进入判断
		#moshan:其功能有三
		#moshan:对于某些参数是不允许有值的(-s), 如果是该参数, 且有值, 则告警退出
		#moshan:对于某些参数只能允许某些值(-t), 如果是这些参数, 且指定的值是规定之外的, 则告警退出
		#moshan:对于某些参数必须要指定值(-ep), 如果是这些参数, 且未指定值, 则告警退出
		f_logging "WARN" "${script_name} : The \"${3}\" is invalid value for \"${2}\". Try \"${script_name} --help\" for more information."
		exit
	elif [ "${opt_tmp}x" == "1x" ]
	then
		#moshan:对于某些参数是不允许与其他参数一起使用的, 如果是该参数, 且与其他参数一起使用, 则告警退出
		f_logging "WARN" "${script_name} : The \"${2}=${3}\" option cannot be used with other options. Try \"${script_name} --help\" for more information."
		exit
	elif [ "${opt_tmp}x" == "2x" ]
	then
		#moshan:对于某些参数是一定要与-t|--type参数一起使用, 如果是该参数, 且没有与-t|--type一起使用, 则告警退出
		f_logging "WARN" "${script_name} : Please use the \"-t${4} or --type${4}\" option when using the \"${2}\" option. Try \"${script_name} --help\" for more information."
		exit
	elif [ "${opt_tmp}x" == "3x" -a "${3}x" == "x" ]
	then
		#moshan:指定某些参数而不指定值, 直接告警退出
		f_logging "WARN" "${script_name} : The \"${3}\" is invalid value for \"${2}\". Try \"${script_name} --help\" for more information."
		exit
	else
		#moshan:if判断一个参数是否是这种格式:-t=, 或者-t 或者-t=value, 三种格式
		if [ "$(sed "s#^.*=##g" <<< "${opt_tmp}" 2>/dev/null)x" == "x" ]
		then
			#moshan:如果是这种格式-t=, 有等号没有值的, 直接报错退出
			f_logging "WARN" "${script_name} : The \"Null value\" is invalid value for \"${parm}\". Try \"${script_name} --help\" for more information."
			exit
		elif [ "$(sed "s#${parm}##g" <<< "${opt_tmp}" 2>/dev/null)x" == "x" ]
		then
			#moshan:如果是这种格式, -t, 直接返回一个空值, 表示该选项无值
			parm_var=""
		else
			#moshan:如果以上都不是, 则表示-t=value, 直接返回value的值
			parm_var="$(echo ${all_var}|awk -F"${parm}=" '{print $2}'|awk '{print $1}')"
		fi
	fi
}

function f_test_var_num()
{
	#moshan:检查输入的值是否是纯数字
	is_num="$(sed 's#[0-9]##g' <<< "${1}")"
	if [ "${is_num}x" == "x" ] 
	then
		return 1
	else
		return 0
	fi
}

function f_test_var_ok()
{
	#moshan: 判断输入的值是否与定义的值
	var_tmp_array=(${1})
	var_tmp="${2}"
	diff_count=0
	for ((diff=0;diff<${#var_tmp_array[@]};diff++))
	do
		if [ "${var_tmp}x" != "${var_tmp_array[${diff}]}x" ]
		then
			diff_count=$((${diff_count}+1))
		fi
	done
	if [ "${diff_count}x" == "${#var_tmp_array[@]}x" ]
	then
		return_str="diff"
	else
		return_str=""
	fi
}
