work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
. ${work_dir}/function/test_script_process.sh
. ${work_dir}/function/remote_send_comm.sh
function f_mgr_install()
{
	f_logging "INFO" "Starting install MGR..." "1" "1"|tee -a ${log_file}
	#moshan:判断MySQL的安装是否已经正常安装, 若未正常安装, 报出错误并不继续往下进行.
	if [ "${1}x" == "1x" ]
	then
		echo 0 > ${mark_file}
	else
		f_logging "ERROR" "MySQL is not installed correctly." "2" "0"|tee -a ${log_file}
		return
	fi
	#moshan: 配置MGR
	install_mgr_sql="set global super_read_only=0;set sql_log_bin = 0;grant replication slave on *.* to ${repl_info[0]}@'${repl_info[3]}' identified by '${repl_info[1]}';set sql_log_bin = 1;change master to master_user='${repl_info[0]}',master_password='${repl_info[1]}' for channel 'group_replication_recovery';install plugin group_replication SONAME 'group_replication.so';"
	#moshan: 启动MGR, master(首个节点启动视为master)与slave启动方式不一样
	start_mgr_sql="set global group_replication_bootstrap_group=on;start group_replication;set global group_replication_bootstrap_group=off;"
	if [ "${2}x" == "mgr_slavex" ]
	then
		start_mgr_sql="set global group_replication_allow_local_disjoint_gtids_join=on;start group_replication;"
		while :
		do
			f_test_script_process "${mysql_cluster_host[0]}"
			state="${?}"
			echo "install_mgr_:state:${state}" >> ${log_file}
			if [ "${state}x" == "0x" ]
			then
				echo "state:${state}"
				#moshan: 接收f_test_script_process函数的返回值, 0|1
				#moshan: 因为只是将mgr master的主机地址传给该函数, 0表示mgr master节点的脚本都运行结束, 1表示mgr master节点的脚本还在运行
				#moshan: 判断安装mgr master的进程是否还在运行, 如果还在运行则不对mgr slave进行安装, 待进程不运行后, 判断安装的状态
				sleep 1
				if [ ! -f "${tmp_file}" ]
				then
					f_logging "ERROR" "Check that the MGR master node installation failed for MGR and EXIT" "2" "0"|tee -a ${log_file}
					exit
				else
					master_mgr_install_state="$(cat ${tmp_file})"
					[ -f "${tmp_file}" ] && rm -f ${tmp_file}
					if [ "${master_mgr_install_state}x" == "1x" ]
					then
						f_logging "INFO" "Now, starting install MGR..."|tee -a ${log_file}
						sleep 5
						break
					elif [ "${master_mgr_install_state}x" == "0x" ]
					then
						f_logging "ERROR" "Check that the MGR master node initialization failed and EXIT" "2" "0"|tee -a ${log_file}
						exit
					elif [ "${master_mgr_install_state}x" == "2x" ]
					then
						f_logging "ERROR" "Check that the MGR master node startup failed and EXIT" "2" "0"|tee -a ${log_file}
						exit
					fi
				fi
			else
				#moshan: 如果是mgr_slave则先判断mgr_master是否已经正常安装, 若未正常安装则等待5秒继续判断
				f_logging "WARN" "But check that the MGR master node has not started normally, so it will wait for five seconds to check the status of the MGR master again."|tee -a ${log_file}
				sleep 5
			fi
		done
		unset state master_mgr_install_state
	fi
	${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -e "${install_mgr_sql}" 2>/dev/null
	if [ "${?}x" == "0x" ]
	then
		f_logging "INFO" "The installation is complete for MGR..."|tee -a ${log_file} 
		f_logging "INFO" "Starting MGR..."|tee -a ${log_file} 
		${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -e "${start_mgr_sql}" 2>/dev/null
		state="${?}"
		if [ "${state}x" == "0x" ] 
		then
			#moshan: 安装成功后, 各节点将打印MGR信息
			master_hostname="$(${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} performance_schema -NBe "select rgm.MEMBER_HOST from replication_group_member_stats rgms join replication_group_members rgm on rgms.MEMBER_ID=rgm.MEMBER_ID;" 2>/dev/null)"
			f_logging "INFO" "Successful startup for MGR. "|tee -a ${log_file}
			check_mgr_state="select * from performance_schema.replication_group_members;"
			echo > ${tmp_file}
			echo -e "\033[33m++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
			echo "Localhost is [$(hostname)], master_host is [${master_hostname}]" >> ${tmp_file}
			echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
			echo >> ${tmp_file}
			${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -NBe "${check_mgr_state}" >> ~/.tmp 2>/dev/null
			echo >> ${tmp_file}
			echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
			echo >> ${tmp_file}
			echo -ne "\033[33m"
			cat ${tmp_file}
			echo -ne "\033[0m"
			[ -f "${tmp_file}" ] && rm -f ${tmp_file}
			echo "1" > ${mark_file}
		else
			f_logging "ERROR" "Startup failed for MGR" "2" "0"|tee -a ${log_file}|tee -a ${log_file} 
			echo 2 > ${mark_file}
		fi
	else
		f_logging "ERROR" "Initialization failed for MGR and EXIT" "2" "0"|tee -a ${log_file}
		echo 0 > ${mark_file}
	fi
	unset state install_mgr_sql start_mgr_sql
	if [ "${localhost_ip}x" == "${mysql_cluster_host[0]}x" ]
	then
		#moshan: 如果是mgr master则将mark文件发送到mgr slave的tmp文件
		for ((i=1;i<${#mysql_cluster_host[@]};i++))
		do
			f_remote_send_comm ${mysql_cluster_host[${i}]} "${mark_file}" "${tmp_file}"
		done
		if [ "$(cat ${mark_file})x" != "1x" ]
		then
			#moshan: 1位正常状态值，0|2为错误状态值
			exit
		fi
	fi
}

