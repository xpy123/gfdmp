work_dir="/data/git/gfdmp"
. ${work_dir}/function/remote_run_comm.sh
function f_test_script_process()
{
	script_process_host=($1)
	#moshan: 判断脚本进程是否还在, 因为是通过远程放在后台运行的
	for ((j=0;j<${#script_process_host[@]};j++))
	do
		#moshan: 判断上一步后台运行的进程是否还在(gfdmp), 均为0表示都已经运行完成(包括正常运行完成或者错误退出)
		#moshan: 接收f_remote_run_comm函数的返回值, 0|1
		f_remote_run_comm "${script_process_host[${j}]}" "ps"
		eval install_state[${j}]="${?}"
	done
	echo "${#script_process_host[@]}:${install_state[@]}" >> ${log_file}
	if [ "$(grep -o 0 <<< "${install_state[@]}" 2>/dev/null|grep -c 0)x" != "${#script_process_host[@]}x" ]
	then
		unset j install_state script_process_host
		#moshan: 上一个for循环是用install_state数组接收每个主机的进程状态值, 0表示已经运行完毕, 1表示还在执行
		#moshan: 该步骤是判断install_state数组的值, 如果全部是0(即0的个数与主机数相等), 则表示所有节点的脚本运行结束, 则返回0
		#moshan: 反之还有某些节点还在运行脚本, 则返回1
		return 1
	else
		unset j install_state script_process_host
		return 0
	fi
}


