function f_show_info()
{
	#moshan:显示软件版本作者信息
	echo -e "\033[32m"
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "       Management platform info for MySQL"
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "         Version: V_${ver}"
	echo "         Author : ${author}"
	echo "         MySQL  : ${support_mysql_version}"
	echo "         QQ     : ${qq}"
	echo "         Mail   : ${mail}"
	echo -e "\033[0m"
}

