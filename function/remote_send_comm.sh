work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
function f_remote_send_comm()
{
	remote_host_ip="${1}"
	s_path="${2}"
	d_path="${3}"
	rsync -avz -e "ssh -p ${remote_host_port}" ${s_path} ${remote_host_user}@${remote_host_ip}:${d_path} >/dev/null 2>>${log_file}
	if [ "${?}x" != "0x" ]
	then
		f_logging "WARN" "Failed to transfer data [s_path:${s_path}, d_path:${d_path}] for [${localhost_ip}] to [${remote_host_ip}]." "2" >>${log_file}
	fi
	unset remote_host_ip s_path d_path
}

