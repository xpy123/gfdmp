work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
. ${work_dir}/function/remote_send_comm.sh
function f_mysql_repl()
{
	#moshan: 搭建复制
	mysql_master="${mysql_cluster_host[0]}:${remote_host_port}:${remote_host_user}"
	mysql_master_info=($(tr ":" " " <<< "${mysql_master}"))
	mysql_base_dir="${2}/base"
	mysql_sock_file="${2}/data/${repl_info[3]}/mysqld.sock"
	for ((i=1;i<${#mysql_cluster_host[@]};i++))
	do
		eval mysql_slave[$((${i}-1))]="${mysql_cluster_host[${i}]}:${remote_host_port}:${remote_host_user}"
	done
	if [ "${1}x" == "mysql_masterx" ]
	then
		#moshan: 如果是master则创建复制账户且将gtid获取到.
		f_logging "INFO" "The master node is creating a replication user [${repl_info[1]}@'${repl_info[4]}']..."|tee -a ${log_file}
		comm="grant replication slave on *.* to ${repl_info[1]}@'${repl_info[4]}' identified by '${repl_info[2]}';"
		${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -e "${comm}" 2>/dev/null
		comm="show master status;"
		${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -e "${comm}" 2>/dev/null|awk '{print $3}' > ${mysql_data_dir}/master_gtid.log
		for ((i=0;i<${#mysql_slave[@]};i++))
		do
			#moshan: 将gtid相关信息发送到slave机器的mysql数据目录下
			client_info=($(tr ":" " " <<< "${mysql_slave[${i}]}"))
			f_remote_send_comm "${client_info[0]}" "${mysql_data_dir}/master_gtid.log" "${mysql_data_dir}/master_gtid.log"
		done
	elif [ "${1}x" == "mysql_slavex" ]
	then
		#moshan: 搭建复制
		f_logging "INFO" "Creating replication relationship from MySQL master ${mysql_master_info[0]}:${repl_info[3]}..."|tee -a ${log_file}
		while :
		do
			#moshan: 判断master是否将gtid相关信息提供到本地数据目录下
			if [ -f "${mysql_data_dir}/master_gtid.log" ]
			then
				break
			else
				sleep 5
			fi
		done
		${mysql_base_dir}/bin/mysql -u${repl_info[1]} -p"${repl_info[2]}" -h${mysql_master_info[0]} -P${repl_info[3]} -NBe "select 1" >/dev/null 2>&1
		if [ "${?}x" != "0x" ]
		then
			#moshan: 判断复制用户是否正常
			f_logging "ERROR" "Sorry, ERROR 1045 (28000): Access denied for user '${repl_info[1]}'@'${mysql_master_info[0]}' (using password: YES)" "2" "0"
			unset mysql_master client_info comm mysql_master_info
			return
		fi
		#moshan: 在slave 上创建复制用户, 并开始搭建复制
		comm="grant replication slave on *.* to ${repl_info[1]}@'${repl_info[4]}' identified by '${repl_info[2]}';"
		${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -e "${comm}" 2>/dev/null
		comm="change master to master_host=\"${mysql_master_info[0]}\",master_user=\"${repl_info[1]}\",master_password=\"${repl_info[2]}\",master_port=${repl_info[3]},master_auto_position=1;"
		${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -e "${comm}" 2>/dev/null
		comm="stop slave;reset master;set global gtid_purged='$(cat ${mysql_data_dir}/master_gtid.log)';"
		${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -e "${comm}" 2>/dev/null
		comm="start slave;"
		${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -e "${comm}" 2>/dev/null
		sleep 5
		#moshan: 打印复制信息, 看看是否正常
		echo > ${tmp_file}
		echo >> ${tmp_file}
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
		echo "Created a replication for ${localhost_ip}:${install_port} to ${mysql_master_info[0]}:${repl_info[3]}..." >> ${tmp_file}
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
		echo "Getting replication status information..." >> ${tmp_file}
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
		${mysql_base_dir}/bin/mysql -u${mysql_admin_user} -p"${install_password}" -S ${mysql_sock_file} -e "show slave status\G" 2>/dev/null|grep -i "Running:" >> ${tmp_file}
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ${tmp_file}
		echo >> ${tmp_file}
		echo -ne "\033[33m"
		cat ${tmp_file}
		echo -ne "\033[0m"
		[ -f "${tmp_file}" ] && rm -f ${tmp_file}
		unset mysql_master client_info comm mysql_master_info
	fi
}
