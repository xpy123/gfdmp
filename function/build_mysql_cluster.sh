work_dir="/data/git/gfdmp"
. ${work_dir}/function/logging.sh
. ${work_dir}/function/remote_run_comm.sh
. ${work_dir}/function/remote_send_comm.sh
. ${work_dir}/function/test_script_process.sh
function f_build_mysql_cluster()
{
	#f_test_ssh "0" "${localhost_ip}"
	for ((i=0;i<${#mysql_cluster_host[@]};i++))
	do
		if [ "${1}x" == "mgrx" ]
		then
			type="install_mysql_mgr"
			if [ "${i}x" == "0x" ]
			then
				type_role="mgr_master"
			else
				type_role="mgr_slave"
			fi
		elif [ "${1}x" == "mysqlx" ]
		then
			type="install_mysql_semi"
			if [ "${i}x" == "0x" ]
			then
				type_role="mysql_master"
			else
				type_role="mysql_slave"
			fi
		fi
		if [ "${localhost_ip}x" != "${mysql_cluster_host[${i}]}x" ]
		then
			#moshan: 同步必要的文件到slave节点, 本机除外
			echo 0 > ${mark_file}
			f_logging "INFO" "Synchronizing gfdmp related files of ${localhost_ip} server to ${mysql_cluster_host[${i}]} server."|tee -a ${log_file}
			f_remote_run_comm "${mysql_cluster_host[${i}]}" "[ ! -d '${work_dir}' ] && mkdir -p ${work_dir};[ -f '${tmp_file}' ] && rm -f ${tmp_file} || echo > /dev/null"
			f_remote_send_comm "${mysql_cluster_host[${i}]}" "${work_dir}/" "${work_dir}/"
		fi
		#moshan: 开始搭建集群
		f_remote_run_comm "${mysql_cluster_host[${i}]}" "gfdmp -t=${type} -idir=${install_dir} -P=${install_port} -p=${install_password} -V=${install_ver} --type-role=${type_role}"|tee -a ${log_file} &
	done
	for ((i=0;i<${#mysql_cluster_host[@]};i++))
	do
		f_test_script_process "${mysql_cluster_host[*]}"
		#moshan: 接收f_test_script_process函数的返回值, 0|1, 0表示所有节点的脚本都运行结束, 1表示还有部分节点的脚本还在运行
		#moshan: 如果是1, 则将重置循环变量i=0, 且sleep 10秒, 然后continue, 继续等待，直到所有节点的脚本都运行结束
		if [ "${?}x" == "1x" ]
		then
			i=0
			sleep 10
			continue
		fi
		#moshan: 查看各节点是否正常安装了
		f_remote_run_comm "${mysql_cluster_host[${i}]}" "cat"
		install_state="${?}"
		if [ "${install_state}x" != "1x" ]
		then
			eval error_host[${i}]="${mysql_cluster_host[${i}]}"
		fi
	done
	unset i install_state
	if [ "${#error_host[@]}x" == "0x" ]
	then
		f_logging "INFO" "All nodes have completed the installation of mysql cluster."|tee -a ${log_file}
	else
		#moshan: 如果有其中一个节点安装失败了, 将会回滚所有操作(卸载安装的mysql)
		#moshan: 卸载前会询问用户, 10s内输入N|n或者CTRL + C可取消回滚操作
		for ((j=0;j<${#error_host[@]};j++))
		do
			if [ "${error_host[${j}]}x" == "x" ]
			then
				#moshan: 判断是否是空, 因为有可能存在某个节点已经正常安装, 所以不予以赋值, 即为空, 如果是空值则continue
				continue
			fi
			f_logging "ERROR" "${error_host[${j}]}:initialization failed for MySQL." "2" "0"|tee -a ${log_file}
			type=uninstall
		done
		f_logging "WARN" "Now rolling back all operations, or you can ENTER [N|n|Y|nCTRL+C] to terminate/continue this operation." "2"|tee -a ${log_file}
		echo -en "\033[33mPlease ENTER your choice within ten seconds [N|n|Y|y|CTRL+C ]:\033[0m"|tee -a ${log_file}
		read -t 10 chioce
		if [ "${chioce}x" == "Nx" -o "${chioce}x" == "nx" ]
		then
			f_logging "WARN" "You ENTER [${chioce}] to abandon this rollback all operations." "2"|tee -a ${log_file}
			return
		elif [ "${chioce}x" == "Yx" -o "${chioce}x" == "yx" ]
		then
			f_logging "WARN" "You ENTER [${chioce}] to continue this rollback all operations." "2"|tee -a ${log_file}
			for ((i=0;i<${#mysql_cluster_host[@]};i++))
			do
				#moshan: 回滚操作, 即卸载MySQL
				f_remote_run_comm "${mysql_cluster_host[${i}]}" "gfdmp -t=${type} -idir=${install_dir} -P=${install_port} -p=${install_password} -V=${install_ver}"|tee -a ${log_file}
			done
		fi
	fi
	unset j chioce error_host
}
