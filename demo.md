#搭建半同步集群
```
[root@centos-1 gfdmp]# gfdmp -V=5.7 -P=3306 -p=zz802366 -idir=/data/mysql


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       Management platform info for MySQL
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         Version: V_3.0
         Author : MoShan
         MySQL  : 5.6 or 5.7
         QQ     : 1005155691
         Mail   : mo_shan@yeah.net

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         MySQL current version : 5.7
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
1.  Backup full data for MySQL
2.  Backup increment data for MySQL
3.  Recover data to MySQL
4.  Recover data to MySQL and Create Replication
5.  Initialization profile
6.  Install MySQL
7.  Uninstall MySQL
8.  Install MySQL and Create MySQL Group Replication
9.  Install MySQL and Create Replication
10. EXIT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Please ENTER your choice [1-10]:

gfdmp /data/git/gfdmp > 9
[2019-05-14 19:36:25] [INFO] [172.20.10.8] Synchronizing gfdmp related files of 172.20.10.8 server to 172.20.10.9 server.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Install info for MySQL on 172.20.10.8
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      MySQL Version     : 5.7
      MySQL port        : 3306
      MySQL password    : zz802366
      MySQL install dir : /data/mysql
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-05-14 19:36:26] [INFO] [172.20.10.8] Starting install MySQL...
[2019-05-14 19:36:26] [INFO] [172.20.10.8] Decompressing MySQL installation package...
[2019-05-14 19:36:59] [INFO] [172.20.10.8] Decompression successful...
[2019-05-14 19:36:59] [INFO] [172.20.10.8] Initializing for MySQL...
[2019-05-14 19:36:59] [INFO] [172.20.10.8] Synchronizing gfdmp related files of 172.20.10.8 server to 172.20.10.10 server.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Install info for MySQL on 172.20.10.9
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      MySQL Version     : 5.7
      MySQL port        : 3306
      MySQL password    : zz802366
      MySQL install dir : /data/mysql
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-05-14 19:37:08] [INFO] [172.20.10.9] Starting install MySQL...
[2019-05-14 19:37:08] [INFO] [172.20.10.9] Decompressing MySQL installation package...
[2019-05-14 19:37:40] [INFO] [172.20.10.9] Decompression successful...
[2019-05-14 19:37:40] [INFO] [172.20.10.9] Initializing for MySQL...
[2019-05-14 19:37:42] [INFO] [172.20.10.8] Initialization successful for MySQL...
[2019-05-14 19:37:42] [INFO] [172.20.10.8] Starting MySQL...
[2019-05-14 19:37:49] [INFO] [172.20.10.9] Initialization successful for MySQL...
[2019-05-14 19:37:49] [INFO] [172.20.10.9] Starting MySQL...
[2019-05-14 19:37:51] [INFO] [172.20.10.8] Successful startup for MySQL
[2019-05-14 19:37:51] [INFO] [172.20.10.8] This old password root@localhost:/7Ji&yBp9rGr
[2019-05-14 19:37:51] [INFO] [172.20.10.8] Changing password root@localhost...
[2019-05-14 19:37:53] [INFO] [172.20.10.9] Successful startup for MySQL
[2019-05-14 19:37:53] [INFO] [172.20.10.9] This old password root@localhost:j,<j5-Afx-U)
[2019-05-14 19:37:53] [INFO] [172.20.10.9] Changing password root@localhost...

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Install info for MySQL on 172.20.10.10
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      MySQL Version     : 5.7
      MySQL port        : 3306
      MySQL password    : zz802366
      MySQL install dir : /data/mysql
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-05-14 19:37:57] [INFO] [172.20.10.10] Starting install MySQL...
[2019-05-14 19:37:57] [INFO] [172.20.10.10] Decompressing MySQL installation package...
[2019-05-14 19:38:03] [INFO] [172.20.10.8] Change password successfully for root@localhost
[2019-05-14 19:38:03] [INFO] [172.20.10.8] The installation is complete for MySQL 5.7!
[2019-05-14 19:38:03] [INFO] [172.20.10.8] This new password root@localhost:zz802366


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
172.20.10.8:Operation command information...
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Connect to MySQL : /data/mysql/base/bin/mysql -uroot -p"zz802366" -S /data/mysql/data/3306/mysqld.sock

Shutdown to MySQL: /data/mysql/base/bin/mysqladmin -uroot -p"zz802366" -S /data/mysql/data/3306/mysqld.sock shutdown

Start to MySQL   : bash /data/mysql/mysqld_3306

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-05-14 19:38:03] [INFO] [172.20.10.8] The master node is creating a replication user [repl@'%']...
[2019-05-14 19:38:03] [INFO] [172.20.10.9] Change password successfully for root@localhost
[2019-05-14 19:38:03] [INFO] [172.20.10.9] The installation is complete for MySQL 5.7!
[2019-05-14 19:38:04] [INFO] [172.20.10.9] This new password root@localhost:zz802366


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
172.20.10.9:Operation command information...
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Connect to MySQL : /data/mysql/base/bin/mysql -uroot -p"zz802366" -S /data/mysql/data/3306/mysqld.sock

Shutdown to MySQL: /data/mysql/base/bin/mysqladmin -uroot -p"zz802366" -S /data/mysql/data/3306/mysqld.sock shutdown

Start to MySQL   : bash /data/mysql/mysqld_3306

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-05-14 19:38:04] [INFO] [172.20.10.9] Creating replication relationship from MySQL master 172.20.10.8:3306...


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Created a replication for 172.20.10.9:3306 to 172.20.10.8:3306...
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Getting replication status information...
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-05-14 19:38:20] [INFO] [172.20.10.10] Decompression successful...
[2019-05-14 19:38:21] [INFO] [172.20.10.10] Initializing for MySQL...
[2019-05-14 19:38:32] [INFO] [172.20.10.10] Initialization successful for MySQL...
[2019-05-14 19:38:32] [INFO] [172.20.10.10] Starting MySQL...
[2019-05-14 19:38:34] [INFO] [172.20.10.10] Successful startup for MySQL
[2019-05-14 19:38:34] [INFO] [172.20.10.10] This old password root@localhost:ew0bs_sP2yzg
[2019-05-14 19:38:34] [INFO] [172.20.10.10] Changing password root@localhost...
[2019-05-14 19:38:44] [INFO] [172.20.10.10] Change password successfully for root@localhost
[2019-05-14 19:38:44] [INFO] [172.20.10.10] The installation is complete for MySQL 5.7!
[2019-05-14 19:38:44] [INFO] [172.20.10.10] This new password root@localhost:zz802366


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
172.20.10.10:Operation command information...
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Connect to MySQL : /data/mysql/base/bin/mysql -uroot -p"zz802366" -S /data/mysql/data/3306/mysqld.sock

Shutdown to MySQL: /data/mysql/base/bin/mysqladmin -uroot -p"zz802366" -S /data/mysql/data/3306/mysqld.sock shutdown

Start to MySQL   : bash /data/mysql/mysqld_3306

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

[2019-05-14 19:38:44] [INFO] [172.20.10.10] Creating replication relationship from MySQL master 172.20.10.8:3306...


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Created a replication for 172.20.10.10:3306 to 172.20.10.8:3306...
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Getting replication status information...
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


```
